<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    Route::resource('blog', 'BlogsController');
    Route::resource('callback', 'CallbacksController');
    Route::resource('comments', 'CommentsController');
    Route::resource('photos', 'PhotoTraningsController');
    Route::resource('psychologist', 'PsychologistsController');
    Route::resource('services', 'ServicesController');
    Route::resource('services-main', 'ServiceDescController');
    Route::resource('sliders', 'SlidersController');

    Route::get('callback/approve/{id}', 'CallbacksController@approve');
    Route::get('callback/decline/{id}', 'CallbacksController@decline');

});
