<?php

namespace App\Admin\Controllers;

use App\Models\ServiceDesc;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ServiceDescController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Наши услуги')
            ->description('блок на главной')
            ->body($this->grid());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ServiceDesc);
        $grid->disableCreateButton();
        $grid->id('ID');
        $grid->name('Название');
        $grid->image('Фото')->lightbox(['zooming' => true]);
        $grid->created_at('Создано');

        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->disableView();
        });
        return $grid;
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать')
            ->body($this->form()->edit($id));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ServiceDesc);

        $form->display('ID');
        $form->text('name', 'Название');
        $form->editor('description', 'Описание');
        $form->image('image', 'Основное изображение')->move('/main')->uniqueName();
        $form->display('Created at');
        $form->display('Updated at');

        return $form;
    }
}
