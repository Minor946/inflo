<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Comments;
use App\Models\Services;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class CommentsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Комментарии')
            ->description('список управления')
            ->body($this->grid());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Comments);

        $grid->actions(function ($actions) {
            $actions->disableView();
        });
        $grid->id('ID');
        $grid->name('ФИО');
        $grid->town('Город');
        $grid->comment('Комментарий');
        $grid->image('Фото')->lightbox(['zooming' => true]);

        $grid->column('page', 'Страница')->display(function () {
            if ($this->page == Services::TYPE_CHILD) {
                return "<span class='label label-warning'>Детская</span>";
            }
            if ($this->page == Services::TYPE_FAMILY) {
                return "<span class='label label-success'>Семейная</span>";
            }
            if ($this->page == Services::TYPE_GROUP) {
                return "<span class='label label-danger'>Групповая</span>";
            }
        })->sortable();

        $grid->created_at('Создано');

        return $grid;
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Изменить')
            ->description('комментарий')
            ->body($this->form()->edit($id));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Comments);

        $form->display('ID');

        $form->text('name', 'ФИО');
        $form->text('town', 'Город');
        $form->text('comment', 'Комментарий');

        $form->select('page', 'Страница')->options(Services::getTypeList());

        $form->image('image', 'Фото')->move('/comment')->uniqueName();

        return $form;
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Добавить комментарий')
            ->body($this->form());
    }
}
