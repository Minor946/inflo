<?php

namespace App\Admin\Controllers;

use App\Models\Sliders;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class SlidersController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Слайдеры')
            ->description('на главной')
            ->body($this->grid());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Sliders);

        $grid->id('ID');
        $grid->title('Заголовок');
        $grid->url('Ссылка');
        $grid->placeholder('Подпись');
        $grid->image('Фото')->lightbox(['zooming' => true]);

        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        return $grid;
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Изменить')
            ->body($this->form()->edit($id));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Sliders);

        $form->text('title', 'Заголовок');
        $form->text('url', 'ссылка');
        $form->text('placeholder', 'подпись');

        $status = [
            Sliders::STATUS_SHOW => 'Показывать',
            Sliders::STATUS_HIDE => 'Скрыть',
        ];

        $form->select('status', 'Статус')->options($status);

        $form->number('position', 'Позиция');

        $form->image('image', 'Изображение слайдера')->move('/sliders-main')->uniqueName();

        return $form;
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Добавить')
            ->description('слайдер')
            ->body($this->form());
    }
}
