<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Psychologists;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class PsychologistsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Психологи')
            ->description('панель управления')
            ->body($this->grid());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Psychologists);

        $grid->id('#');
        $grid->column('name', 'Имя')->sortable();
        $grid->column('position', 'Должность')->sortable();
        $grid->image('Фото')->lightbox(['zooming' => true]);
        $grid->created_at('Создано');

        $grid->disableFilter();
        $grid->disableExport();

        $grid->actions(function ($actions) {
            $actions->disableDelete();
        });

        return $grid;
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Психолог')
            ->description('профиль')
            ->body($this->detail($id));
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Psychologists::findOrFail($id));

        $show->id('ID');
        $show->name('ФИО');
        $show->position('Должность');
        $show->experience('Опыт и образование');
        $show->specialization('Области специализации');
        $show->more('Дополнительная специфика');
        $show->image('Фото')->image();
        return $show;
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Изменить')
            ->description('Изменить данные психолога')
            ->body($this->form()->edit($id));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Psychologists);

        $form->display('ID');

        $form->text('name', 'ФИО');
        $form->text('position', 'Должность');
        $form->textarea('experience', 'Опыт и образование')->rows(5);
        $form->textarea('specialization', 'Области специализации')->rows(5);
        $form->text('more', 'Дополнительная специфика');

        $form->number('position_list', 'Позиция');

        $form->image('image', 'Фото')->move('/psychologists')->uniqueName();

        $form->multipleImage('certificates', 'Сертификаты')->move('/psychologists/certificates')->uniqueName()->removable();

        return $form;
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Добавить')
            ->description('Добавить психолога')
            ->body($this->form());
    }
}
