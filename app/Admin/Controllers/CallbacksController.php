<?php

namespace App\Admin\Controllers;

use App\Models\Callbacks;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class CallbacksController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Обратные звонки')
            ->description('список')
            ->body($this->grid());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Callbacks);

        $grid->model()->orderBy('status', 'asc');
        $grid->disableCreateButton();

        $grid->id('ID');

        $grid->column('type', 'Тип')->display(function () {
            $type = CallBacks::getTypeLabel($this->type);
            return "<span class='label label-{$type['type']}'>{$type['name']}</span>";
        })->sortable();

        $grid->column('phone', 'Телефон');
        $grid->column('name', 'Имя');
        $grid->column('email', 'Почта');
        $grid->column('msg', 'Сообщение');
        $grid->column('status', 'Статус')->display(function () {
            if ($this->status == CallBacks::STATUS_NEW) {
                return "<span class='label label-info'>Новый</span>";
            }
            if ($this->status == CallBacks::STATUS_CHECK) {
                return "<span class='label label-success'>Обработан</span>";
            }
            if ($this->status == CallBacks::STATUS_DECLINE) {
                return "<span class='label label-danger'>Отклонен</span>";
            }
        })->sortable();

        $grid->created_at('Создано');
        $grid->filter(function ($filter) {
            // Sets the range query for the created_at field
            $filter->between('created_at', 'Created Time')->datetime();
        });

        $grid->actions(function ($actions) {
            $actions->disableEdit();
            $actions->disableDelete();
            $actions->disableView();

            if (CallBacks::find($actions->getKey())->status == CallBacks::STATUS_NEW) {
                $actions->prepend('<a href="/admin/callback/decline/' . $actions->getKey() . '"><i class="fa fa-times text-red"></i></a>');
                $actions->prepend('<a href="/admin/callback/approve/' . $actions->getKey() . '"><i class="fa fa-check"></i></a>');
            }
        });

        return $grid;
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Обратный звонок')
            ->body($this->detail($id));
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Callbacks::findOrFail($id));

        $show->id('ID');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать')
            ->body($this->form()->edit($id));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Callbacks);

        $form->display('ID');
        $form->display('Created at');
        $form->display('Updated at');

        return $form;
    }


    public function approve($id)
    {
        $model = CallBacks::find($id);
        if ($model->status == CallBacks::STATUS_NEW) {
            $model->status = CallBacks::STATUS_CHECK;
            $model->save();
        }
        return redirect('/admin/callback');
    }


    public function decline($id)
    {
        $model = CallBacks::find($id);
        if ($model->status == CallBacks::STATUS_NEW) {
            $model->status = CallBacks::STATUS_DECLINE;
            $model->save();
        }
        return redirect('/admin/callback');
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Добавить')
            ->body($this->form());
    }
}
