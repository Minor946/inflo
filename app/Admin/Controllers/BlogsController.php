<?php

namespace App\Admin\Controllers;

use App\Models\Blogs;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class BlogsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Статьи')
            ->description('список статей')
            ->body($this->grid());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Blogs);

        $grid->id('ID');
        $grid->title('Заголовок');
        $grid->short_desc('Красткое описание');
        $grid->image('Фото')->lightbox(['zooming' => true]);
        $grid->created_at('Создано');

        return $grid;
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Статья')
            ->body($this->detail($id));
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Blogs::findOrFail($id));

        $show->id('ID');

        $show->title('Заголовок');
        $show->short_desc('Красткое описание');
        $show->description('Статья')->html();
        $show->image('Фото')->image();

        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование')
            ->body($this->form()->edit($id));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Blogs);

        $form->display('ID');
        $form->text('title', 'Заголовок');
        $form->text('short_desc', 'Краткое описание');
        $form->editor('description', 'Статья');

        $form->image('image', 'Основное изображение')->move('/post')->uniqueName();

        $status = [
            Blogs::STATUS_SHOW => 'Показывать',
            Blogs::STATUS_HIDE => 'Скрыть',
        ];

        $popular = [
            Blogs::POPULAR_OF => 'Обычная',
            Blogs::POPULAR_ON => 'Популярная',
        ];

        $form->select('status', 'Статус')->options($status);

        $form->select('popular', 'Популярная')->options($popular);

        $form->multipleImage('sliders', 'Фотографии на слайдер')->move('/post/sliders')->uniqueName()->removable();

        $form->saving(function (Form $form) {
            $form->model()->alias = str_slug($form->title, '_');
        });

        $form->display('Created at');
        $form->display('Updated at');

        return $form;
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создать')
            ->description('статью')
            ->body($this->form());
    }
}
