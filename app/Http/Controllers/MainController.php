<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 23.09.2018
 * Time: 19:03
 */

namespace App\Http\Controllers;


use App\Models\Blogs;
use App\Models\Callbacks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MainController extends Controller
{

    public function index()
    {
        return view('index');
    }


    public function psychologists()
    {
        return view('psychologists');
    }

    public function service($type)
    {
        if (!empty($type)) {
            switch ($type) {
                case "child":
                    return view('service.child');
                case "family":
                    return view('service.family');
                case "group":
                    return view('service.group');
                default :
                    return abort(404);
            }
        }
        return abort(404);
    }

    public function blog($alias = null)
    {
        if (!empty(Blogs::whereAlias($alias))) {
            return view('blog_single', ['blog_single' => Blogs::whereAlias($alias)]);
        }
        $blogs = Blogs::where('status', Blogs::STATUS_SHOW)
            ->orderBy('created_at', 'desc')->take(6)->get();

        $count = Blogs::where('status', Blogs::STATUS_SHOW)
            ->orderBy('created_at', 'desc')->get();

        return view('blog', ['blogs' => $blogs, 'count' => $count]);
    }

    public function about()
    {
        return view('about');
    }

    public function club()
    {
        return view('club');
    }

    public function trainings()
    {
        return view('trainings');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        $data = $request->all();
        if (!empty($data) && $request->isMethod('post')) {
            $offset = $request->input('limit');
            $blogs = Blogs::where('status', Blogs::STATUS_SHOW)
                ->orderBy('created_at', 'desc')->take(6)->offset($offset)->get();
            $out = "";
            foreach ($blogs as $blog) {
                $out .= view('components.blog.item', ['blog' => $blog]);
            }
            return response()->json(['data' => $out, 'count' => count($blogs)]);
        }
        return response()->json(null);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function callback(Request $request)
    {
        $data = $request->all();
        if (!empty($data) && $request->isMethod('post')) {
            $callback = new Callbacks();
            $callback->type = $request->input('type');
            $callback->name = $request->input('name');
            $callback->phone = $request->input('phone');
            if (!empty($request->input('email'))) {
                $callback->email = $request->input('email');
            }
            if (!empty($request->input('msg'))) {
                $callback->msg = $request->input('msg');
            }
            $callback->status = Callbacks::STATUS_NEW;
            $callback->save();

            Mail::send('emails.callback', ['callback' => $callback], function ($m) use ($callback) {
                $m->from('admin@inflowww.com', 'Inflowww.com');
                $m->to('lumineaa@gmail.com', 'Inflowww.com')
                    ->cc('info@inflowww.com')
                    ->subject("Новый звонок: " . Callbacks::getTypeLabelById($callback->type));
            });

            return response()->json(['status' => 'true']);
        }
        return response()->json(['status' => 'false']);
    }

}