<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $type
 * @property int $price
 * @property int $price_lesson
 * @property string $price_course
 * @property string $discount
 * @property int $page
 * @property string $created_at
 * @property string $updated_at
 */
class Services extends Model
{
    protected $table = 'services';

    const TYPE_GROUP = 0;
    const TYPE_CHILD = 1;
    const TYPE_FAMILY = 2;

    /**
     * @var array
     */
    protected $fillable = ['type', 'price', 'price_lesson', 'price_course', 'discount', 'page', 'created_at', 'updated_at'];

    public static function getTypeList()
    {
        return [
            self::TYPE_GROUP => "Групповая",
            self::TYPE_CHILD => "Детская",
            self::TYPE_FAMILY => "Семейная"
        ];
    }
}
