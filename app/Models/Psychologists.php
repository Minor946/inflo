<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $position
 * @property string $image
 * @property string $experience
 * @property string $specialization
 * @property string $more
 * @property array $certificates
 * @property int $position_list
 * @property string $created_at
 * @property string $updated_at
 */
class Psychologists extends Model
{
    protected $table = 'psychologists';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'position', 'image', 'experience',
        'specialization', 'more', 'certificates',
        'created_at', 'updated_at'
    ];


    public function setCertificatesAttribute($pictures)
    {
        if (is_array($pictures)) {
            $this->attributes['certificates'] = json_encode($pictures);
        }
    }

    public function getCertificatesAttribute($pictures)
    {
        return json_decode($pictures, true);
    }

}
