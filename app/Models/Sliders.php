<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $url
 * @property string $placeholder
 * @property string $image
 * @property int $status
 * @property int $position
 * @property string $created_at
 * @property string $updated_at
 */
class Sliders extends Model
{

    const STATUS_SHOW = 0;
    const STATUS_HIDE = 1;

    protected $table = 'sliders';

    /**
     * @var array
     */
    protected $fillable = ['title', 'url', 'placeholder',
        'image', 'status', 'position', 'created_at', 'updated_at'];

}
