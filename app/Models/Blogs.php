<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $short_desc
 * @property string $description
 * @property string $alias
 * @property string $image
 * @property integer $views
 * @property integer $popular
 * @property integer $status
 * @property string $sliders
 * @property string $created_at
 * @property string $updated_at
 */
class Blogs extends Model
{

    const STATUS_SHOW = 0;
    const STATUS_HIDE = 1;


    const POPULAR_ON = 0;
    const POPULAR_OF = 1;

    protected $table = 'blogs';

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'short_desc', 'description',
        'alias', 'image', 'views', 'status', 'popular',
        'created_at', 'updated_at', 'sliders'
    ];

    public static function whereAlias($alias)
    {
        $blog = Blogs::where('alias', $alias)->first();
        if (!empty($blog)) {
            return $blog;
        }
        return null;
    }

    public function setSlidersAttribute($sliders)
    {
        if (is_array($sliders)) {
            $this->attributes['sliders'] = json_encode($sliders);
        }
    }

    public function getSlidersAttribute($sliders)
    {
        return json_decode($sliders, true);
    }

}
