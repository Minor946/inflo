<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceDesc extends Model
{
    protected $table = 'service_desc';
}
