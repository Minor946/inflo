<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $town
 * @property string $comment
 * @property int $page
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 */
class Comments extends Model
{

    protected $table = 'comments';

    /**
     * @var array
     */
    protected $fillable = ['name', 'town', 'comment', 'page', 'image', 'created_at', 'updated_at'];

}
