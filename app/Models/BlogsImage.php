<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $image
 * @property string $anchor
 * @property string $created_at
 * @property string $updated_at
 */
class BlogsImage extends Model
{

    protected $table = 'blogs_image';

    /**
     * @var array
     */
    protected $fillable = ['image', 'anchor', 'created_at', 'updated_at'];

}
