<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $type
 * @property string $photo
 * @property string $alt
 * @property string $created_at
 * @property string $updated_at
 */
class PhotoTranings extends Model
{
    protected $table = 'photo_tranings';

    /**
     * @var array
     */
    protected $fillable = ['type', 'photo', 'alt', 'created_at', 'updated_at'];

    public function setPhotoAttribute($pictures)
    {
        if (is_array($pictures)) {
            $this->attributes['photo'] = json_encode($pictures);
        }
    }

    public function getPhotoAttribute($pictures)
    {
        return json_decode($pictures, true);
    }
}
