<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $type
 * @property string $msg
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class Callbacks extends Model
{

    const STATUS_NEW = 0;
    const STATUS_CHECK = 1;
    const STATUS_DECLINE = 2;


    const TYPE_MAIN = 0;
    const TYPE_PAGE_FAMILY = 1;
    const TYPE_PAGE_GROUP = 2;
    const TYPE_PAGE_GROUP_FREE = 3;
    const TYPE_PAGE_CHILD = 4;
    const TYPE_PAGE_TRAINING = 5;
    const TYPE_MENU_QUESTION = 6;
    const TYPE_MENU_CONSULTATION = 7;
    const TYPE_MENU_TRAINING = 8;

    const TYPE_TRAINING_1 = 10;
    const TYPE_TRAINING_2 = 11;
    const TYPE_TRAINING_3 = 12;
    const TYPE_TRAINING_4 = 13;
    const TYPE_TRAINING_5 = 14;
    const TYPE_TRAINING_6 = 15;
    const TYPE_TRAINING_7 = 16;

    protected $table = 'callbacks';

    /**
     * @var array
     */
    protected $fillable = ['name', 'phone', 'email', 'type', 'msg', 'status', 'created_at', 'updated_at'];


    public static function getTypeList()
    {
        return [
            self::TYPE_MAIN => "Главная",
            self::TYPE_PAGE_FAMILY => "Семейная",
            self::TYPE_PAGE_GROUP => "Групповая",
            self::TYPE_PAGE_GROUP_FREE => "Групповая FREE",
            self::TYPE_PAGE_CHILD => "Детская",
            self::TYPE_PAGE_TRAINING => "Тренинги",
            self::TYPE_MENU_QUESTION => "Вопрос",
            self::TYPE_MENU_CONSULTATION => "Консультация",
            self::TYPE_MENU_TRAINING => "Тренинги меню",

            self::TYPE_TRAINING_1 => "Групповое занятие",
            self::TYPE_TRAINING_2 => "Групповой месяц",
            self::TYPE_TRAINING_3 => "Групповой курс",
            self::TYPE_TRAINING_4 => "Семейное занятие",
            self::TYPE_TRAINING_5 => "Семейный месяц",
            self::TYPE_TRAINING_6 => "Семейный курс",
            self::TYPE_TRAINING_7 => "Детский курс",

        ];
    }


    public static function getTypeLabelById($type)
    {

        if (isset($type) && array_key_exists($type, self::getTypeList())) {
            return self::getTypeList()[$type];
        }
        return null;
    }

    public static function getTypeLabel($type)
    {
        switch ($type) {
            case self::TYPE_PAGE_GROUP:
            case self::TYPE_PAGE_GROUP_FREE:
            case self::TYPE_TRAINING_1:
            case self::TYPE_TRAINING_2:
            case self::TYPE_TRAINING_3:
                return ['type' => 'success', 'name' => self::getTypeLabelById($type)];
            case self::TYPE_PAGE_FAMILY:
            case self::TYPE_TRAINING_4:
            case self::TYPE_TRAINING_5:
            case self::TYPE_TRAINING_6:
                return ['type' => 'info', 'name' => self::getTypeLabelById($type)];
            case self::TYPE_PAGE_CHILD:
            case self::TYPE_TRAINING_7:
                return ['type' => 'primary', 'name' => self::getTypeLabelById($type)];
            case self::TYPE_MAIN:
                return ['type' => 'warning', 'name' => self::getTypeLabelById($type)];
            case self::TYPE_PAGE_TRAINING:
            case self::TYPE_MENU_QUESTION:
            case self::TYPE_MENU_CONSULTATION:
            case self::TYPE_MENU_TRAINING:
                return ['type' => 'info', 'name' => self::getTypeLabelById($type)];
            default:
                return null;
        }
    }

}
