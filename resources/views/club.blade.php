@extends('layouts.layout')

@section('title', 'Клуб')

@section('content')
    <div class="container">
        <section>
            <h2 class="margin-height-20 margin-bottom-60">ЧТО ТАКОЕ КЛУБ <img class="logo-h2"
                                                                              src="{{url('/images/icons/logo.svg')}}">
            </h2>
            <div class="club-container">
                <div class="row" align="center">
                    <div class="col-md-4">
                        <img src="{{url('/images/icons/mosaic-0.png')}}">
                        <p>пространство для<br>
                            саморазвития<br>
                            и нетворкинга</p>
                    </div>
                    <div class="col-md-4">
                        <img src="{{url('/images/icons/mosaic-1.png')}}">
                        <p>комфортная, современная<br>
                            студия в тихом районе<br>
                            с удобной парковкой</p>
                    </div>
                    <div class="col-md-4">
                        <img src="{{url('/images/icons/mosaic-2.png')}}">
                        <p>групповая поддержка<br>
                            каждую 2-ую среду<br>
                            месяца</p>
                    </div>
                </div>
                <div class="row justify-content-center" align="center">
                    <div class="col-md-4">
                        <img src="{{url('/images/icons/mosaic-3.png')}}">
                        <p>групповые скидки на треннеров<br>
                            мирового уровня в сфере<br>
                            психологии, бизнеса и<br>
                            саморазвития</p>
                    </div>
                    <div class="col-md-4">
                        <img src="{{url('/images/icons/mosaic-4.png')}}">
                        <p>доступ к обширной<br>
                            библиотеке с книгами<br>
                            по психологии и<br>
                            саморазвитию</p>
                    </div>
                </div>
            </div>
            <div class="margin-top-20" align="center">
                <p>
                    <strong>
                        Чтобы стать членом нашего клуба, нужно пройти один<br>из наших дейсвующих тренингов или курсов.
                    </strong>
                </p>
            </div>
        </section>
        <section>
            @include('components.courses.view')
        </section>
    </div>
@endsection
