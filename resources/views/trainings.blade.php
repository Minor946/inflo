@extends('layouts.layout')

@section('title', 'Online тренинги и курсы')

@section('content')
    <div class="container">
        <section>

            <div class="row">
                    <div class="col-md-6 order-2 order-md-1">
                        <div class="landing-label margin-height-30">
                            <h2>ONLINE</h2>
                            <h2>ТРЕНИНГИ</h2>
                            <h2>И КУРСЫ</h2>
                        </div>
                        <p class="margin-height-30">
                            Ознакомится с сожержанием треннингов и курсов<br> вы можете по ссылкам
                        </p>
                        <p><a href="{{ url('/service/group') }}">Позитивная психология для Вас</a></p>
                        <p><a href="{{ url('/service/family') }}">Позитивная психология для семейных пар</a></p>
                        <p><a href="{{ url('/service/child') }}">Позитивная психология для подростков</a></p>
                    </div>
                    <div class="col-md-6 order-1 order-md-2">
                        <div class="video_wrapper video_wrapper_full js-videoWrapper">
                            <iframe class="videoIframe"
                                    src="{{config('youtube_traning')}}"
                                    frameborder="0" allowTransparency="true"
                                    width='475px' height='267px'>
                            </iframe>
                        </div>
                        <p class="margin-height-20">
                            Все наши тренинги и курсы вы можете пройти дистанционно скачав приложение UDEMY в PlayMarket
                            или
                            в AppStore
                        </p>
                        <div class="row">
                            <div class="col-6 col-md-3">
                                <a href="{{config('app-store')}}"><img src="{{url('/images/icons/app-store.svg')}}"></a>
                            </div>
                            <div class="col-6  col-md-3">
                                <a href="{{config('google-pay')}}"><img src="{{url('/images/icons/google-play.svg')}}"></a>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
        <section>
            @include('components.callback.callback', ['id'=>'form-callback-o','callback_type'=>\App\Models\Callbacks::TYPE_PAGE_TRAINING])
        </section>
        <section>
            @include('components.courses.view')
        </section>
    </div>
@endsection
