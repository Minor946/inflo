@extends('layouts.layout')

@section('title', "Психологи")

@section('content')
    <div class="container">
        <div class="row" align="center">
            <?php $psychologists = \App\Models\Psychologists::orderBy('position', 'desc')->get();;?>
            <?php foreach ($psychologists as $key => $psycho):?>

            <section class="psychologist-section" style="<?=($key > 0) ? "margin-top:0;" : ""?>">
                @include('components.psychologists.item', ['psycho'=>$psycho])
            </section>
            <?php endforeach;?>
        </div>
    </div>
@endsection

