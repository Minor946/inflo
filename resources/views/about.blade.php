@extends('layouts.layout')

@section('title', 'О себе')

@section('content')
    @include('components.about.view')
@endsection
