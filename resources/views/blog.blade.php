@extends('layouts.layout')

@section('title', "Статьи")

@section('content')
    <section>
        <div class="container blog-container">
            <div class="row" align="center" id="blogContainer">
                <?php foreach ($blogs as $blog):?>
                @include('components.blog.item', ['blog'=>$blog])
                <?php endforeach;?>
            </div>
            <?php if(count($count) > 6):?>
            <div align="center">
                <a href="javascript:void(0);" class="btn btn-main-style margin-height-10" id="add-more">Смотреть еще</a>
            </div>
            <?php endif;?>
        </div>
    </section>
@endsection
