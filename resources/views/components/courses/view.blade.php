<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 10/9/2018
 * Time: 10:47 AM
 */

use App\Models\Sliders;

$trainings = Sliders::where('status', Sliders::STATUS_SHOW)->orderBy('position', 'asc')->get();
?>

@if(count($trainings) > 0)
    <div>
        <h2 class="margin-bottom-60">ДЕЙСТВУЮЩИЕ КУРСЫ И ТРЕНИНГИ</h2>
        <div class="swiper-container" id="slider-training" align="center">
            <div class="swiper-wrapper">
                <?php /** @var Sliders $training */
                foreach ($trainings as $training):?>
                <div class="swiper-slide">
                    <div class="box-course2">
                        <img src="{{url('/storage/'.$training->image)}}">
                        <div class="box-course2-content">
                            <h3>
                                <?=$training->title?>
                            </h3>
                            <p class="text-muted"><?=$training->placeholder?></p>
                        </div>
                        <a href="{{ url($training->url) }}" class="btn btn-main-style margin-height-20" target="_blank">Подробнее</a>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var mySwiperTraining = new Swiper('#slider-training', {
                direction: 'horizontal',
                slidesPerView: <?=(count($trainings) > 4) ? 4 : count($trainings)?>,
                pagination: {
                    el: '.swiper-pagination',
                },
            });

            $(window).resize(function () {
                var ww = $(window).width();
                if (ww <= 768) {
                    mySwiperTraining.params.slidesPerView = 1;
                } else {
                    mySwiperTraining.params.slidesPerView = <?=(count($trainings) > 4) ? 4 : count($trainings)?>
                }
                mySwiperTraining.update();
            });
            $(window).trigger('resize');
        });
    </script>
@endif