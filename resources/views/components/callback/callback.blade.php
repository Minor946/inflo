<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 9/28/2018
 * Time: 2:20 PM
 */
?>
<div class="row">
    <div class="col-md-6">
        <div class="callback-text">
            <h4 class="label-title">
                У ВАС ОСТАЛИСЬ ВОПРОСЫ?<br>
                ЗАПОЛНИТЕ ФОРМУ И МЫ<br>
                СВЯЖЕМСЯ С ВАМИ<br>
            </h4>
        </div>
    </div>
    <div class="col-md-6">
        <div class="callback-box">
            <div class="container">
                {!! Form::open(['url' => '/callback', 'id'=>$id]) !!}
                {{Form::token()}}
                <div class="form-group">
                    {{ Form::text("name", "", array_merge(['class' => 'form-control', 'placeholder'=>'ваше имя', 'required' => true])) }}
                </div>
                <div class="form-group">
                    {{ Form::text("phone", "", array_merge(['class' => 'form-control', 'placeholder'=>'телефон', 'type'=>'phone', 'required' => true])) }}
                </div>
                <div class="form-group">
                    {{ Form::email("email", "", array_merge(['class' => 'form-control',  'placeholder'=>'e-mail', 'required' => true])) }}
                </div>
                {{ Form::hidden('type', $callback_type)}}
                <div align="center">
                    {{Form::submit('Отправить',['class' => 'btn btn-submit'])}}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
