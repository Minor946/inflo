<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 10/8/2018
 * Time: 3:51 PM
 */

?>
<div class="container">
    <div class="landing-label">
        <h2>ОСНОВАТЕЛЬ</h2>
        <img src="{{url('/images/shape_color.svg')}}" class="margin-height-15" style="margin-left: -30px;">
    </div>
    <div class="row">
        <div class="col-md-6 margin-height-50 order-2 order-md-1">
            <p class="margin-top-40">
                Здравствуйте, меня зовут Гузяль Якубова, мне 34 года, я дипломированный практикующий психотерапевт, мама
                двух, почти уже взрослых сыновей, и просто счастливая женщина… Хочу вам рассказать, почему я решила
                открыть Студию Позитивной Психологии.
            </p>
            <p><strong>Так вот… моя история…</strong></p>
            <a href="{{ url('/about') }}"
               class="btn btn-main-style btn-main-white btn-fix-size margin-top-10">Подробнее</a>
        </div>
        <div class="col-md-6 order-1 order-md-2" align="center">
            <img src="{{url('/images/psychologists/guzal-colors.png')}}" class="margin-left-35 img-full creater_img">
        </div>
    </div>
</div>
