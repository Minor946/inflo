<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 10/8/2018
 * Time: 3:50 PM
 */

?>
<div class="container" id="can-get">
    <h2>ЧТО ВЫ У НАС МОЖЕТЕ ПОЛУЧИТЬ</h2>
    <div class="get-block">
        <div align="left">
            <div class="get-icon get-icon-1">
                <img src="{{url('/images/get-icon/icon-main-1.svg')}}">
                <p class="animated hidden">саморазвитие</p>
            </div>
        </div>
        <div class="margin-top-45">
            <div class="get-icon get-icon-2">
                <img src="{{url('/images/get-icon/icon-main-2.svg')}}">
                <p class="animated hidden">раскроете свой<br>
                    потенциал</p>
            </div>
        </div>

        <div class="margin-bottom-15 margin-right-15">
            <div class="get-icon get-icon-3">
                <img src="{{url('/images/get-icon/icon-main-3.svg')}}">
                <p class="animated hidden">хорошее<br>
                    настроение</p>
            </div>
        </div>

        <div align="right">
            <div class="get-icon get-icon-4">
                <img src="{{url('/images/get-icon/icon-main-4.svg')}}">
                <p class="animated hidden">откроете мир<br>
                    эмоций</p>
            </div>
        </div>

    </div>
    <div class="get-block">
        <div>
            <div class="get-icon get-icon-5">
                <img src="{{url('/images/get-icon/icon-main-5.svg')}}">
                <p class="animated hidden">улучшите<br>
                    взаимоотношения</p>
            </div>
        </div>
        <div class="margin-top-15 margin-right-80" align="right">
            <div class="get-icon get-icon-6">
                <img src="{{url('/images/get-icon/icon-main-6.svg')}}">
                <p class="animated hidden">откроете новые<br>
                    горизонты </p>
            </div>
        </div>

        <div class="margin-top-45">
            <div class="get-icon get-icon-7">
                <img src="{{url('/images/get-icon/icon-main-7.svg')}}">
                <p class="animated hidden">научитесь<br>
                    радоваться<br>
                    жизни</p>
            </div>
        </div>
    </div>

    <div class="get-block">
        <div class="margin-top-75">
            <div class="get-icon get-icon-8">
                <img src="{{url('/images/get-icon/icon-main-8.svg')}}">
                <p class="animated hidden">научитесь<br>
                    наполнятся<br>
                    энергией</p>
            </div>
        </div>

        <div style="margin-top: -35px;">
            <div class="get-icon get-icon-9">
                <img src="{{url('/images/get-icon/icon-main-9.svg')}}">
                <p class="animated hidden">начнете творить</p>
            </div>
        </div>

        <div class="margin-top-5">
            <div class="get-icon get-icon-10">
                <img src="{{url('/images/get-icon/icon-main-10.svg')}}">
                <p class="animated hidden">обретете<br>
                    уверенность<br>
                    в себе</p>
            </div>
        </div>

        <div class="margin-top-25">
            <div class="get-icon get-icon-11">
                <img src="{{url('/images/get-icon/icon-main-11.svg')}}">
                <p class="animated hidden">освоите техники<br>
                    раслабления </p>
            </div>
        </div>

    </div>
</div>
