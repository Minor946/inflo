<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 10/8/2018
 * Time: 3:53 PM
 */

?>

<div class="container">
    <div class="row">
        <div class="col-md-7 ">
            <div class="swiper-container" id="slider-studio" align="center">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="{{url('/images/blog_default.jpg')}}" width="100%">
                    </div>
                    <div class="swiper-slide">
                        <img src="{{url('/images/blog_default.jpg')}}" width="100%">
                    </div>
                    <div class="swiper-slide">
                        <img src="{{url('/images/blog_default.jpg')}}" width="100%">
                    </div>
                    <div class="swiper-slide">
                        <img src="{{url('/images/blog_default.jpg')}}" width="100%">
                    </div>
                </div>
                <div class="swiper-pagination pagination-position control-bullet inline-flex margin-right-25"></div>
            </div>
            <div class="row">
                <div class="col-md-8">
                </div>
                <div class="col-md-4 inline-flex margin-top-25">
                    <div class="flat-arrow flat-arrow-left">
                        <img src="{{url('/images/icons/arrow-left.png')}}">
                    </div>
                    <div class="flat-arrow flat-arrow-right">
                        <img src="{{url('/images/icons/arrow-right.png')}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <h2>О СТУДИИ <span><img src="{{url('/images/icons/hr.svg')}}" class="margin-bottom-5"></span></h2>
            <p>
                Студия Позитивной Психологии IN FLOW - это пространство для самосовершенствования, где люди могут
                делится сложностями, опытом, находить ответы, получать поддержку, обмениваться положительными эмоциями,
                смеяться, получать новые знания, создавать новые привычки, находить новых друзей.
            </p>
            <p>
                Место где люди понимают разницу между делать все по правилам и быть личностью, следуя своему внутреннему
                голосу. Место, где каждый может узнать себя получше, свои приоритеты, свои ценности, учится делать
                осознанный выбор и творить свою судьбу! Общество, в котором расцветает потенциал, в котором интересно и
                весело, в котором: гармония, красота, баланс, успешность, сила воли, саморазвитие - это ценность!
            </p>
        </div>
    </div>
</div>
