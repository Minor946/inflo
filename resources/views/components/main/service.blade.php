<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 10/5/2018
 * Time: 3:49 PM
 */
$services = \App\Models\ServiceDesc::all();
?>
@if(isset($services))
    <div class="container">
        <h2 class="margin-height-30">НАШИ УСЛУГИ</h2>
        <div class="mob-hide">
            <ul class="nav nav-tabs nav-fill no-border margin-height-30" id="myTab" role="tablist">
                @foreach($services as $key => $item)
                    <li class="nav-item-service">
                        <a class="nav-link-service <?=($key == 0) ? "active" : ''?>" href="#tab-service-<?=$item->id?>"
                           id="service-tab-<?=$item->id?>" data-toggle="tab"
                           role="tab"
                           aria-controls="service-<?=$item->id?>" aria-selected=" <?=($key == 0) ? "true" : 'false'?>">
                            <?=$item->name?>
                            <img src="{{url('/images/shape_color.svg')}}">
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content" id="myTabContent">
                @foreach($services as $key => $item)
                    <div class="tab-pane fade show <?=($key == 0) ? "active" : ''?>" id="tab-service-<?=$item->id?>"
                         role="tabpanel" aria-labelledby="service-tab-<?=$item->id?>">
                        <div class="row">
                            <div class="col-md-6" align="center">
                                <img src="{{url('/storage/'.$item->image)}}" class="img-service">
                            </div>
                            <div class="col-md-5 margin-height-30">
                                <p> <?=$item->description?> </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="swiper-container mob-hidden" id="slider-services" align="center">
            <div class="swiper-wrapper">
                @foreach($services as $key => $item)
                    <div class="swiper-slide">
                        <div class="service-mob-label" align="left">
                            <h3 class="service-mob-text"><?=$item->name?></h3>
                            <img src="{{url('/storage/'.$item->image)}}" class="img-service-mob">
                        </div>
                        <div align="left">
                            <p> <?=$item->description?> </p>
                        </div>
                    </div>
            @endforeach
            <div class="swiper-pagination"></div>
        </div>
    </div>

    </div>
@endif
