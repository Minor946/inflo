<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 10/8/2018
 * Time: 3:57 PM
 */

use App\Models\Sliders;

$sliders = Sliders::where('status', Sliders::STATUS_SHOW)->orderBy('position', 'asc')->get();
?>
@if(isset($sliders))
    <div class="container first-slide">
        <div class="row">
            <div class="col-md-4 order-2 order-md-1 margin-top-120">
                <p>Психологическая помощь</p>
                <p>Групповая терапия</p>
                <p>Консультирование семейных пар</p>
                <p>Работа с подростками</p>
                <p>Бизнес коучинг</p>
            </div>
            <div class="col-md-8 order-1 order-md-2" style="position:relative;">
                <div class="slider-img" id="main-slider">
                    @foreach($sliders as $key=>$item)
                        <img src="{{url('/storage/'.$item->image)}}" data-slide="<?=$key?>"
                             width="100%" <?=($key > 0) ? 'class="display-none"' : ''?>>
                    @endforeach
                    <div class="swiper-container" id="slider-main" align="center">
                        <div class="swiper-wrapper">
                            @foreach($sliders as $key=>$item)
                                <div class="swiper-slide">
                                    <div class="margin-top-25">
                                        <h3><?=$item->title?></h3>
                                        <p class="text-muted"><?=$item->placeholder?></p>
                                        <a href="{{ url($item->url) }}"
                                           class="btn btn-main-style margin-height-20 btn-wight-180">Подробнее</a>
                                    </div>
                                </div>
                            @endforeach
                            <div class="content" align="center">
                                <div class="swiper-pagination pagination-main control-bullet control-bullet-pos"></div>
                            </div>
                        </div>
                        <div class="inline-flex margin-height-25">
                            <div class="flat-arrow flat-arrow-main-left">
                                <img src="{{url('/images/icons/arrow-left.png')}}">
                            </div>
                            <div class="flat-arrow flat-arrow-main-right">
                                <img src="{{url('/images/icons/arrow-right.png')}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
