<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 10/10/2018
 * Time: 10:26 AM
 */

?>

<div class="container">
    <h2 class="margin-height-30">ПОДХОДЫ ИСПОЛЬЗУЕМЫЕ В РАБОТЕ</h2>
    <div align="center">
        <div class="row methods-block">
            <div class="col-12 col-md-6  order-2 order-md-1">
                <img src="{{url('/images/icons/rectangle-18.png')}}" height="388" class="image-methods">
                <a href="javascript:void(0);" class="close-tab hidden"><img src="{{url('/images/icons/close.png')}}"
                                                                            width="100%"></a>
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active mob-hide" role="tabpanel" id="first-tab">
                        <h2 class="label-light">ПОДХОДЫ<br>
                            ИСПОЛЬЗУЕМЫЕ<br>
                            В РАБОТЕ</h2>
                        <img src="{{url('/images/icons/rectangle-18.png')}}" height="388">
                    </div>
                    <div class="tab-pane padding-top-10 fade show" align="left" id="v-cognitively" role="tabpanel"
                         aria-labelledby="v-pills-cognitively">
                        <p>
                            Этот метод психотерапии обращается к сознанию и помогает осознать автоматические привычки и
                            освободиться от стереотипов и предвзятых представлений, которые лишают нас свободы выбора и
                            подталкивают действовать по шаблону.
                        </p>
                    </div>
                    <div class="tab-pane padding-top-10 fade show" align="left" id="v-focus" role="tabpanel"
                         aria-labelledby="v-pills-focus">
                        <p>
                            Подход, который фокусируется на привычках обращения человека со своими эмоциями. Помогает
                            осознать привычный способ поведения человека в том или ином эмоциональном состоянии и начать
                            выбирать новый более продуктивный способ.
                        </p>
                    </div>
                    <div class="tab-pane padding-top-10 fade show" align="left" id="v-positive" role="tabpanel"
                         aria-labelledby="v-pills-positive">
                        <p>
                            Позитивная психология – это направление психологии, которое занимается исследованием
                            положительных аспектов психики человека. Когда человек уже понимает свои мыслительные и
                            эмоциональные привычки - он может начать менять их в новом позитивном направлении. Начать
                            развивать в себе новые аспекты личности, такие как: оптимизм, поток, доверие, прощение и
                            солидарность. Эта область исследований стремится раскрыть природные способности человека и
                            сделать опыт жизни более удовлетворительным.
                        </p>
                    </div>
                    <div class="tab-pane padding-top-10 fade show" align="left" id="v-gestalt" role="tabpanel"
                         aria-labelledby="v-pills-gestalt">
                        <p>
                            Гештальт - подход побуждает человека лучше узнать и принять себя – принять таким, какой он
                            есть
                            на самом деле, со своим негативным опытом и социально неприемлемыми качествами, со своими
                            психотравмами и вытесненными конфликтами. Психотерапевт тут выступает лишь в роли
                            проводника,
                            ориентирующегося на желания и возможности клиента, но, с другой стороны, является как бы
                            другом
                            и помощником в трудных, болезненных ситуациях.
                        </p>
                    </div>
                    <div class="tab-pane padding-top-10 fade show" align="left" id="v-emotional" role="tabpanel"
                         aria-labelledby="v-pills-emotional">
                        <p>
                            Эмоциональный интеллект – это развитие таких навыков, как понимание и собственных чувств,
                            так и
                            эмоций окружающих людей. Благодаря этому мы можем эффективно управлять реакцией на чувства
                            других людей и, таким образом, продуктивнее делать свою работу. Главная задача в развитии
                            эмоционального интеллекта – не подавлять или игнорировать трудные эмоции или чувства, но
                            разумно
                            обращаться с ними.

                            Способность человека распознавать эмоции, понимать намерения, мотивацию и желания других
                            людей и
                            свои собственные, а также способность управлять своими эмоциями и эмоциями других людей в
                            целях
                            решения практических задач.
                        </p>
                    </div>
                    <div class="tab-pane padding-top-10 fade show" align="left" id="v-social" role="tabpanel"
                         aria-labelledby="v-pills-social">
                        <p>
                            В основе лежит процедура социальной помощи, оказание поддержки со стороны окружения,
                            ликвидация
                            негативных последствий взаимоотношений, устранение препятствий, мешающих полноценному
                            социальному развитию клиента.
                        </p>
                    </div>
                    <div class="tab-pane padding-top-10 fade show" align="left" id="v-group" role="tabpanel"
                         aria-labelledby="v-pills-group">
                        <p>
                            Этот метод позволяет вам открыть что-то важное о самом себе — о своем поведении, системе
                            мотивации, мире фантазий или о своем бессознательном. Участники могут увидеть более
                            объективную
                            картину своего социального поведения. Как они выглядят в глазах других людей, как проявляют
                            себя
                            в межличностных отношениях. Затем они изучают свое воздействие на окружающих. Здесь
                            происходит
                            осознание скрытых мотивов. Участники могут узнать, почему они делают то, что они делают с
                            другими людьми. Через исследование истории своего развития, пациент понимает происхождение
                            своих
                            паттернов поведения в настоящем.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 padding-top-10 order-1 order-md-2 methods-height" align="left">
                <div class="nav flex-column nav-can-get" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link" id="v-cognitively-tab" onclick="showClose()" data-toggle="pill"
                       href="#v-cognitively"
                       aria-controls="v-pills-home" role="tab" aria-selected="false">
                        Когнитивно-поведенческая психотерапия</a>
                    <a class="nav-link" id="v-focus-tab" onclick="showClose()" data-toggle="pill" href="#v-focus"
                       role="tab"
                       aria-selected="false">Эмоциональная фокусная терапия</a>
                    <a class="nav-link" id="v-positive-tab" onclick="showClose()" data-toggle="pill" href="#v-positive"
                       role="tab"
                       aria-selected="false">Техники Позитивной психологии</a>
                    <a class="nav-link" id="v-gestalt-tab" onclick="showClose()" data-toggle="pill" href="#v-gestalt"
                       role="tab"
                       aria-selected="false">Техники Гештальт терапии</a>
                    <a class="nav-link" id="v-emotional-tab" onclick="showClose()" data-toggle="pill"
                       href="#v-emotional" role="tab"
                       aria-selected="false">Техники Эмоционального интеллекта</a>
                    <a class="nav-link" id="v-social-tab" onclick="showClose()" data-toggle="pill" href="#v-social"
                       role="tab"
                       aria-selected="false">Социальная терапия</a>
                    <a class="nav-link" id="v-group-tab" onclick="showClose()" data-toggle="pill" href="#v-group"
                       role="tab"
                       aria-selected="false">Групповая терапия по Ирвину Ялому</a>
                </div>
            </div>
        </div>
    </div>
</div>

