<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 10/10/2018
 * Time: 12:29 PM
 *
 */
?>

@if(isset($type))
    <?php
    $comments = \App\Models\Comments::where('page', $type)->orderBy('created_at', 'desc')->get();
    ?>
    @if( count($comments) > 0 )
        <div class="swiper-container" id="slider-review" align="center">
            <div class="swiper-wrapper">
                @foreach($comments as $comment)
                    <div class="swiper-slide">
                        <div class="box-review">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="{{url('/storage/'.$comment->image)}}" class="review">
                                </div>
                                <div class="col-md-9">
                                    <p class="margin-top-10">
                                        <strong><?=$comment->name?></strong><br>
                                        <span><?=$comment->town?></span>
                                    </p>
                                </div>
                            </div>
                            <div class="speech-bubble" id="review<?=$comment->id?>">
                                <p><?=$comment->comment?></p>
                                <?php if(strlen($comment->comment) > 280 ):?>
                                <div align="center">
                                    <small><span data-value="review<?=$comment->id?>"
                                                 class="review-more">читать далее</span>
                                    </small>
                                </div>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="swiper-pagination"></div>
        </div>
    @endif
@endif