<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 10/10/2018
 * Time: 12:25 PM
 */

$photos = \App\Models\PhotoTranings::where('type', $type)
    ->orderBy('created_at', 'desc')->take(10)->get();
?>

@if(count($photos) > 0)
    <div class="row">
        <div class="col-md-8">
            <div class="landing-label">
                <h2>ФОТО С ПРОШЕДШИХ ТРЕНИНГОВ</h2>
            </div>
        </div>
        <div class="col-md-4 inline-flex">
            <div class="flat-arrow flat-arrow-left">
                <img src="{{url('/images/icons/arrow-left.png')}}">
            </div>
            <div class="flat-arrow flat-arrow-right">
                <img src="{{url('/images/icons/arrow-right.png')}}">
            </div>
        </div>
    </div>
    <div class="swiper-container" id="slider-photo" align="center">
        <div class="swiper-wrapper">
            @foreach($photos as $photo)
                @foreach($photo->photo as $item_photo)
                    <div class="swiper-slide">
                        <img src="{{url('/storage/'.$item_photo)}}" width="100%">
                    </div>
                @endforeach
            @endforeach
        </div>
    </div>
@endif