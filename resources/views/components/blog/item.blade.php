<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 30.09.2018
 * Time: 23:41
 */

use Carbon\Carbon;
?>

<?php if(isset($blog)):?>
<?php
Carbon::setLocale(app()->getLocale());
$dt = Carbon::parse($blog->created_at);
?>
<a href="{{ url('/blog/'.$blog->alias) }}" class="blog-link margin-full-15">
    <div class="box-blog">
        <div class="box-blog-image">
            <img src="{{url('/storage/'.$blog->image)}}" width="100%">
        </div>
        <div class="box-blog-content">
            <div class="box-blog-title">
                <h3>
                    <?=$blog->title?>
                </h3>
            </div>
            <div class="box-blog-text">
                <?=$blog->short_desc?>
            </div>
            <div class="box-blog-footer">
                {{ $dt->format('d.m.y') }}
            </div>
        </div>
    </div>
</a>
<?php endif;?>
