<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 10/9/2018
 * Time: 10:56 AM
 */
?>
<?php if(isset($populars) && count($populars) > 0):?>
<div class="row">
    <div class="col-md-8">
        <div class="landing-label">
            <h2>ПОПУЛЯРНЫЕ СТАТЬИ</h2>
        </div>
    </div>
    <div class="col-md-4 inline-flex">
        <div class="flat-arrow flat-arrow-left">
            <img src="{{url('/images/icons/arrow-left.png')}}">
        </div>
        <div class="flat-arrow flat-arrow-right">
            <img src="{{url('/images/icons/arrow-right.png')}}">
        </div>
    </div>
</div>
<div class="swiper-container" id="slider-popular" align="center">
    <div class="swiper-wrapper">
        <?php foreach ($populars as $popular) :?>
        <div class="swiper-slide">
            @include('components.blog.item', ['blog'=>$popular])
        </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif;?>
