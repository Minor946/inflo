<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 01.10.2018
 * Time: 0:24
 */

use App\Models\Psychologists;

/**
 * @property Psychologists $psycho
 */
?>

<?php if(isset($psycho)):?>
<div class="psychologist-block" align="center">
    <div>
        <img src="{{url('/storage/'.$psycho->image)}}" class="psychologist-photo" width="100%">
    </div>
    <div class="clearfix"></div>
    <h4 class="label-title"><?=$psycho->name?></h4>
    <h5><?=$psycho->position?></h5>
    <div class="psychologist-content">
        <div class="row" align="left">
            <div class="col-md-6">
                <h5 class="psychologist-label"><strong>Опыт и образование: </strong></h5>
                <p>
                    <?=$psycho->experience?>
                </p>
            </div>
            <div class="col-md-6">
                <h5 class="psychologist-label"><strong>Области специализации: </strong></h5>
                <p><?=$psycho->specialization?></p>
                <h5 class="psychologist-label"><strong>Дополнительная специфика: </strong></h5>
                <p><?=$psycho->more?></p>
                <div class="clearfix"></div>
                <?php if(!empty($psycho->certificates) && count($psycho->certificates) > 0):?>
                <div class="mob-center">
                    <a onclick="openCertificates(<?=$psycho->id?>)"
                       class="btn btn-main-style btn-main-white btn-fix-size margin-top-10">Сертификаты</a>
                </div>
                <div class="certificates<?=$psycho->id?>">
                    <?php foreach ($psycho->certificates as $certificate):?>
                    <a href="{{url('/storage/'.$certificate)}}" class="hidden"><img
                                src="{{url('/storage/'.$certificate)}}" alt="" title=""/></a>
                    <?php endforeach;?>
                </div>
                <?php endif;?>
            </div>
        </div>
        <div align="center">
            <a href="javascript:void(0)" class="btn btn-main-style margin-height-50"
               data-toggle="modal" data-target="#modal-callback"
            >Записаться на консультацию</a>
        </div>
    </div>
</div>
<?php endif;?>