<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 01.10.2018
 * Time: 0:24
 */

use App\Models\Psychologists;

/**
 * @property Psychologists $psycho
 */
?>

<?php if(isset($psycho)):?>
<img src="{{url('/storage/'.$psycho->image)}}">
<h3><?=$psycho->name?></h3>
<p><?=$psycho->position?></p>
<p><strong>Опыт и образование: </strong></p>
<p> <?=$psycho->experience?></p>
<?php endif;?>