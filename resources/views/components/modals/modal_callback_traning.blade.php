<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 10/19/2018
 * Time: 2:29 PM
 */
?>

<div class="modal fade" tabindex="-1" role="dialog" id="<?=$id?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-content-full">
            <div class="callback-box">
                <img src="{{url('/images/icons/close.png')}}" class="close" data-dismiss="modal" aria-label="Close">
                <div class="container">
                    <h2 id="modal-traning-name">Записаться на курс</h2>
                    <div align="center">
                        <p><?=$title?></p>
                        <p class="text-muted"><?=$sub_title?></p>
                    </div>
                    {!! Form::open(['url' => '/callback', 'id'=>'form-'.$id]) !!}
                    {{Form::token()}}
                    <div class="form-group">
                        {{ Form::text("name", "", array_merge(['class' => 'form-control', 'placeholder'=>'ваше имя', 'required' => true])) }}
                    </div>
                    <div class="form-group">
                        {{ Form::text("phone", "", array_merge(['class' => 'form-control', 'placeholder'=>'телефон', 'type'=>'phone', 'required' => true])) }}
                    </div>
                    <div class="form-group">
                        {{ Form::email("email", "", array_merge(['class' => 'form-control',  'placeholder'=>'e-mail', 'required' => true])) }}
                    </div>
                    {{ Form::hidden('type', $callback_type)}}
                    <div align="center">
                        {{Form::submit('Отправить',['class' => 'btn btn-submit'])}}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>