<?php
use App\Models\Blogs;use Carbon\Carbon;

?>
@extends('layouts.layout')
<?php /** @var Blogs $blog_single */
if(isset($blog_single)):?>
@section('title', $blog_single->title)
@section('title-news', $blog_single->title)
@section('description', $blog_single->short_desc)
@section('title', $blog_single->image)

@section('content')
    <?php
    Carbon::setLocale(app()->getLocale());
    $dt = Carbon::parse($blog_single->created_at);
    ?>
    <section>
        <div class="container padding-top-20">
            <div class="landing-label">
                <h2><?=$blog_single->title?></h2>
            </div>
            <div class="row row-blog">
                <div class="col-md-6 order-2 order-md-1">
                    <div class="blog-content">
                        <p>{{ $dt->format('d.m.y') }}</p>
                        <div class="clearfix"></div>
                        <?=$blog_single->description?>
                    </div>
                </div>
                <div class="col-md-6 order-1 order-md-2">
                    <div class="slider" id="sticker">
                        <div class="swiper-container" id="slider-blog">
                            <div class="swiper-wrapper">
                                <?php if(!empty($blog_single->sliders) && count($blog_single->sliders) > 0):?>
                                    <?php foreach ($blog_single->sliders as $slider):?>
                                        <div class="swiper-slide">
                                            <img src="{{url('/storage/'.$slider)}}" class="image-blog">
                                        </div>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="swiper-blog-control">
                            <div class="blog-control">
                                <div class="row">
                                    <div class="col">
                                        <img src="{{url('/images/icons/arrow-left.svg')}}"
                                             class="control-btn control-btn-left">
                                        <img src="{{url('/images/icons/arrow-right.svg')}}"
                                             class="control-btn control-btn-right">
                                    </div>
                                    <div class="col">
                                        <div class="swiper-pagination control-bullet"></div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-8 col-md-6">
                                    <p class="small-text margin-left-20">
                                        Понравилась статья?<br>
                                        Поделитесь в соц сетях
                                    </p>
                                </div>
                                <div class="col-3  col-md-6" align="center">
                                    <a class="facebook-share-button"
                                       href="https://www.facebook.com/sharer/sharer.php?u={{url($_SERVER['REQUEST_URI']) }}"
                                       target="_blank">
                                        <img src="{{url('/images/icons/facebook-share.svg')}}">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section>
                <div class="blog-container">
                    <?php
                    $blogs_popular = Blogs::where('status', Blogs::STATUS_SHOW)->where('popular', Blogs::STATUS_SHOW)
                        ->orderBy('created_at', 'desc')->take(4)->get();
                    ?>
                    <?php if(count($blogs_popular) > 0):?>
                    @include('components.blog.popular', ['populars'=>$blogs_popular])
                    <?php endif;?>
                </div>
            </section>
        </div>
    </section>
    <div id="fb-root"></div>
@endsection
<?php endif;?>