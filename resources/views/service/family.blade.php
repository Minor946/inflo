<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 06.10.2018
 * Time: 16:45
 */

?>

@extends('layouts.layout')

@section('title', "Курс «Позитивная психология» Эмоциональный интеллект для семейных пар")

@section('content')

    <div id="full-page">
        <section class="padding-top-40">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="landing-label">
                            <h2>Курс «Позитивная психология</h2>
                            <h2>+</h2>
                            <h2>Эмоциональный интеллект<br>
                                для семейных пар»</h2>
                        </div>
                        <p class="text-muted">(занятия в группе)</p>
                    </div>
                    <div class="col-md-6">
                        <div class="video_wrapper video_wrapper_full js-videoWrapper">
                            <iframe class="videoIframe js-videoIframe"
                                    src="{{config('youtube_family')}}"
                                    frameborder="0" allowTransparency="true"
                                    width='475px' height='267px'>
                            </iframe>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="margin-top-10">
                            <h3>Вы счастливы? </h3>
                            <p>Если поиск ответа занимает больше 3 секунд и начинается с: «Нуу, ...», значит, вы
                                оказались
                                на
                                нужной страничке.</p>
                            <p>Как, не улыбаясь через силу, достичь постоянного и спокойного, не сиюминутного чувства
                                счастья?</p>
                            <p>Как не бояться доверять и прощать, научиться обращаться со своими эмоциями, понимать их
                                причины и
                                называть своими именами?</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="margin-top-10">
                            <p>Как пробудить своего внутреннего гения, начать верить в себя и ощущать удовлетворение от
                                жизни?</p>
                            <p>Будем постепенно погружаться в поток этих знаний и открывать для себя позитивную
                                психологию. </p>
                            <p><strong>Добро пожаловать на курс Позитивной Психологии!</strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <h2>ЧТО ТАКОЕ ПОЗИТИВНАЯ ПСИХОЛОГИЯ</h2>
                <div class="row margin-top-50">
                    <div class="col-md-6 order-2 order-md-1 margin-top-50">
                        <p>Направление психологии, которое занимается исследованием положительных аспектов психики
                            человека.
                            В отличии от классической психологии, которая ориентируется в основном на проблемы и
                            патологии,
                            основными темами исследований позитивной психологии является то, что способствует достижению
                            счастья людей (например, оптимизм, поток, доверие, прощение и солидарность).</p>

                        <p>Эта область исследований стремится раскрыть природные способности человека и сделать опыт
                            жизни
                            более удовлетворительным и радостным.</p>
                    </div>
                    <div class="col-md-6 order-1 order-md-2">
                        <img src="{{url('/images/landing/family/family_1.png')}}" class="img-full">
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <h2>ДЛЯ КОГО ЭТОТ КУРС</h2>
                <div class="row margin-top-50">
                    <div class="col-md-6" align="center">
                        <img src="{{url('/images/landing/family/family_2.png')}}" class="img-full img-group">
                    </div>
                    <div class="col-md-6  margin-top-50">
                        <ul>
                            <li>
                                Для семейных пар и пар в гражданском браке, которые в
                                отношениях более 3 месяцев и хотят продолжать быть вместе;
                            </li>
                            <li>
                                Для тех, кто хочет освежить отношения, углубить понимание,
                                понять друг друга лучше;
                            </li>
                            <li>Для пары, где оба хотят работать над отношениями;</li>
                            <li>
                                Для пар, которым знакомо чувство разочарования друг в
                                друге, непонимание, одиночество, ощущение отверженности,
                                негативность, грубость, холод, страх, печать, сопротивление,
                                чувство недооцененности, взаимные обвинения,
                                игнорирование;
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <h2>НА НАШЕМ КУРСЕ ВЫ ПОЛУЧИТЕ:</h2>
                <div class="row margin-top-50 justify-content-center">
                    <div class="col-md-3">
                        <ul class="list-styled">
                            <li>Близость</li>
                            <li>Взаимопонимание</li>
                            <li>Уважение</li>
                            <li>Опору и поддержку</li>
                            <li>Доверие</li>
                            <li>Страсть</li>
                            <li>Воодушевление</li>
                            <li>Заботу</li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-styled">
                            <li>Любовь</li>
                            <li>Взаимоподдержку</li>
                            <li>Радость</li>
                            <li>Надежду</li>
                            <li>Оптимизм</li>
                            <li>Взаимный интерес</li>
                            <li>Планирование совместного будущего</li>
                            <li>Общение</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <h2>ПО ОКОНЧАНИЮ КУРСА</h2>
                <div class="row margin-top-50">
                    <div class="col-md-6" align="center">
                        <img src="{{url('/images/landing/family/family_3.png')}}" class="img-full img-group">
                    </div>
                    <div class="col-md-6 margin-top-50">
                        <p class="margin-top-45">
                            Вы будете понимать друг друга глубже на трех уровнях: на уровне интеллекта, на уровне эмоции
                            и
                            на уровне физиологии. Это значит, что вы будете понимать привычки друг друга. Вы сможете
                            понимать эмоции друга друга, понимать что с ними делать и как обращаться друг с другом в том
                            или
                            ином эмоциональном состоянии. И знать физиологические привычки друг друга, чтоб улучшить
                            сексуальную жизнь.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <h2>УСЛОВИЯ И СТОИМОСТЬ</h2>
                <div class="row justify-content-center about-course" align="center">
                    <div class="col-md-10 margin-top-5">
                        <p><strong>Длительность курса: 6 месяцев</strong></p>
                        <p>
                            Минимальное кол-во пар -12, пары будут разделены в 2 разные группы для более продуктивной
                            работы, чтобы
                            половинки друг друга не смущали, но при этом, одновременно прорабатывали одни и те же
                            техники;
                        </p>
                        <p class="margin-top-5 margin-bottom-5"><strong>Стоимоть для пары: 60 000 сом</strong></p>
                        <p>24 занятия по 3 часа), встречи по субботам 4 раза в месяц, 1-ая группа до с 10:00 до 13:00,
                            2-ая
                            группа с
                            14:00 до 17:00;</p>
                    </div>
                </div>
                <div class="row justify-content-center margin-top-15">
                    <div class="box-course-family">
                        <div class="box-course-family-content">
                            <div class="box-course-family-price">
                                <h4>Стоимость одного занятия</h4>
                            </div>
                            <p><strong>2 500 сом</strong></p>
                        </div>
                        <a href="javascript:void(0);"
                           class="btn btn-main-style padding-width-15" data-toggle="modal"
                           data-target="#callback-traning-4">Зарегистрироваться</a>
                    </div>
                    <div class="box-course-family">
                        <div class="box-course-family-content">
                            <div class="box-course-family-price">
                                <h4>Стоимость за месяц</h4><br>
                                <p class="text-muted">(за 4 3-х часовых занятия)</p>
                            </div>
                            <p><strong>9 500 сом</strong></p>
                            <p class="discount">Скидка 8%</p>
                        </div>
                        <a href="javascript:void(0);"
                           class="btn btn-main-style padding-width-15" data-toggle="modal"
                           data-target="#callback-traning-5">Зарегистрироваться</a>
                    </div>
                    <div class="box-course-family">
                        <div class="box-course-family-content">
                            <div class="box-course-family-price">
                                <h4>При оплате за весь курс сразу</h4>
                                <p class="text-muted">(24 занятий по 3 часов)</p>
                            </div>
                            <p><strong>55 000 сом</strong></p>
                            <p class="discount">Скидка 55%</p>
                        </div>
                        <a href="javascript:void(0);"
                           class="btn btn-main-style padding-width-15" data-toggle="modal"
                           data-target="#callback-traning-6">Зарегистрироваться</a>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <h2>КТО ВЕДЕТ КУРС?</h2>
                <div class="row margin-top-50">
                    <?php $model = \App\Models\Psychologists::find(1); ?>
                    <div class="col-md-6" align="center">
                        @include('components.psychologists.item_small', ['psycho'=> $model])
                    </div>
                    <?php $model = \App\Models\Psychologists::find(2); ?>
                    <div class="col-md-6" align="center">
                        @include('components.psychologists.item_small', ['psycho'=> $model])
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <h2>ОТЗЫВЫ</h2>
                <div class="review-main-container">
                    @include('components.blog.review',['type'=>\App\Models\Services::TYPE_FAMILY])
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <div class="blog-container">
                    @include('components.blog.photo',['type'=>\App\Models\Services::TYPE_FAMILY])
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                @include('components.callback.callback', ['id'=>'form-callback-f','callback_type'=>\App\Models\Callbacks::TYPE_PAGE_FAMILY])
            </div>
        </section>
    </div>

    @include('components.modals.modal_callback_traning', ['id'=>'callback-traning-4','callback_type'=>\App\Models\Callbacks::TYPE_TRAINING_4,
'title'=>'«Позитивная психология» + Эмоциональный интеллект для семейных пар', 'sub_title'=>'Одно занятие'])
    @include('components.modals.modal_callback_traning', ['id'=>'callback-traning-5','callback_type'=>\App\Models\Callbacks::TYPE_TRAINING_5,
     'title'=>'«Позитивная психология» + Эмоциональный интеллект для семейных пар', 'sub_title'=>'Один месяц'])
    @include('components.modals.modal_callback_traning', ['id'=>'callback-traning-6','callback_type'=>\App\Models\Callbacks::TYPE_TRAINING_6,
      'title'=>'«Позитивная психология» + Эмоциональный интеллект для семейных пар', 'sub_title'=>'Полный курс'])

@endsection
