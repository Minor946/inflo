<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 06.10.2018
 * Time: 16:46
 */

?>

@extends('layouts.layout')

@section('title', "Курс «Позитивная психология» Эмоциональный интеллект")

@section('content')
    <div id="full-page">
        <section class="padding-top-40">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="landing-label">
                            <h2>Курс «Позитивная психология</h2>
                            <h2>+</h2>
                            <h2>Эмоциональный интеллект»</h2>
                        </div>
                        <p class="text-muted">(занятия в группе)</p>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="video_wrapper video_wrapper_full js-videoWrapper">
                            <iframe class="videoIframe"
                                    src="{{config('youtube_group')}}"
                                    frameborder="0" allowTransparency="true"
                                    width='475px' height='267px'>
                            </iframe>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="margin-top-10">
                            <h3>Вы счастливы? </h3>
                            <p>Если поиск ответа занимает больше 3 секунд и начинается с: «Нуу, ...», значит, вы
                                оказались
                                на
                                нужной страничке.</p>
                            <p>Как, не улыбаясь через силу, достичь постоянного и спокойного, не сиюминутного чувства
                                счастья?</p>
                            <p>Как не бояться доверять и прощать, научиться обращаться со своими эмоциями, понимать их
                                причины и
                                называть своими именами?</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="margin-top-10">
                            <p>Как пробудить своего внутреннего гения, начать верить в себя и ощущать удовлетворение от
                                жизни?</p>
                            <p>Будем постепенно погружаться в поток этих знаний и открывать для себя позитивную
                                психологию. </p>
                            <p><strong>Добро пожаловать на курс Позитивной Психологии!</strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <h2>ЧТО ТАКОЕ ПОЗИТИВНАЯ ПСИХОЛОГИЯ</h2>
                <div class="row margin-top-50">
                    <div class="col-12 col-md-6 order-2 order-md-1 margin-top-50">
                        <p>Направление психологии, которое занимается исследованием положительных аспектов психики
                            человека.
                            В отличии от классической психологии, которая ориентируется в основном на проблемы и
                            патологии,
                            основными темами исследований позитивной психологии является то, что способствует достижению
                            счастья людей (например, оптимизм, поток, доверие, прощение и солидарность).</p>

                        <p>Эта область исследований стремится раскрыть природные способности человека и сделать опыт
                            жизни
                            более удовлетворительным и радостным.</p>
                    </div>
                    <div class="col-12 col-md-6 order-1 order-md-2" align="center">
                        <img src="{{url('/images/landing/group/group_1.png')}}" class="img-full img-group">
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <h2>ДЛЯ КОГО ЭТОТ КУРС</h2>
                <div class="row">
                    <div class="col-12  col-md-6" align="center">
                        <img src="{{url('/images/landing/group/group_2.png')}}" class="img-full img-group">
                    </div>
                    <div class="col-12 col-md-6">
                        <ul class="list-normal">
                            <li>Для того, кто занимается саморазвитием</li>
                            <li>Кто открыл для себя мир эмоций - их природу и разнообразие</li>
                            <li>Кто хочет быть уверенным в себе</li>
                            <li>Кто хочет творить, раскрывать новые горизонты, находить
                                новые таланты
                            </li>
                            <li>Кто стремится улучшить свои взаимоотношения</li>
                            <li>Кто готов к переменам и хочет внедрять в свою жизнь
                                новые привычки счастливого человека
                            </li>
                            <li>Для того, кому важно быть в хорошем настроении</li>
                            <li>Кто хочет получать наслаждение и удовлетворение от жизни</li>
                            <li>Кто хочет раскрывать свой потенциал</li>
                            <li>Кто хочет сделать жизнь наполненную смыслом, целями,
                                юмором, теплыми отношениями
                            </li>
                            <li>Кто готов выполнять практические задания - так как этот курс
                                создан для реальных перемен, а не как теоретический материал
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <h2>ДЛЯ КОГО ЭТОТ КУРС</h2>
                <div class="row margin-top-50">
                    <div class="col-12  col-md-6" align="center">
                        <img src="{{url('/images/landing/group/group_3.png')}}" class="img-full img-group">
                    </div>
                    <div class="col-md-6">
                        <ul class="list-normal">
                            <li>Суть позитивной психологии</li>
                            <li>Основы эмоционального интеллекта с практическими заданиями</li>
                            <li>Основы физиологической связи настроения и гормонов</li>
                            <li>Базу взаимосвязи эмоций и мыслей</li>
                            <li>Практики позитивного человека (медитации, дневник
                                благодарности, практики глубокого дыхания)
                            </li>
                            <li>Техники создания новых привычек, которые меняют убеждения, опыт,
                                а соответственно и жизнь. Как сказал Будда: “Посейте поступок -
                                пожнете привычку, посейте привычку - пожнете характер, посейте
                                характер - пожнете судьбу”
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                @include('components.callback.callback_gift', ['id'=>'form-callback-gf','callback_type'=>\App\Models\Callbacks::TYPE_PAGE_GROUP_FREE])
            </div>
        </section>

        <section>
            <div class="container">
                <h2>ПОЧЕМУ ВАЖНО?</h2>
                <div class="row margin-top-50 group-step margin-bottom-50">
                    <div class="col-12 order-2 order-md-1 col-md-6">
                        <p>
                            Нет ничего плохого в том, чтоб испытывать те или иные эмоции: становится печальными,
                            грустными,
                            чувствовать себя одинокими, ощущать сильную злость или быть подавленным. Все эмоции важны и
                            нужны. Каждый живой человек испытывает эмоции.
                        </p>
                    </div>
                    <div class="col-12  col-md-6 order-1 order-md-2" align="center">
                        <img src="{{url('/images/landing/group/step-1.png')}}" height="120">
                    </div>
                </div>

                <div class="row margin-bottom-50">
                    <div class="col-12  col-md-6" align="center">
                        <img src="{{url('/images/landing/group/step-2.png')}}" height="120">
                    </div>
                    <div class="col-12  col-md-6">
                        <p>
                            Проблемы и сложности начинаются, когда человек застревает в эмоциональном состоянии или
                            отключается от своих эмоций. Например, человек всегда грустный или недовольный. А может
                            агрессивный и заводится с полуслова. А может ничего не доводит до конца или долго не знает
                            чего
                            хочет.
                        </p>
                    </div>
                </div>
                <div class="row margin-bottom-50">
                    <div class="col-12 col-md-6 order-2 order-md-1">
                        <p>
                            Самое худшее, когда человек отказывается чувствовать. Тот самый случай, когда все есть, а
                            ощущения счастья нет. Вот в такие моменты важно выделить время для своего эмоционального
                            состояния. Найти блоки, затруднения, повторяющиеся автоматические реакции, которые
                            продолжают производить одни и те же результаты. Разобраться в своих эмоциях, расставить по
                            полкам, начать понимать откуда берутся эмоции и что полезного они для меня имеют.
                        </p>
                    </div>
                    <div class="col-12 col-md-6 order-1 order-md-2" align="center">
                        <img src="{{url('/images/landing/group/step-3.png')}}" height="120">
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <h2>НА НАШЕМ КУРСЕ ВЫ ПОЛУЧИТЕ:</h2>
                <div class="row margin-top-50">
                    <div class="col-12  col-md-6">
                        <ul class="list-styled">
                            <li>
                                <p>
                                    Мотивацию реализовать ваш внутренний потенциал, разбудите вашего внутреннего гения и
                                    побудить
                                    себя к переменам;
                                </p>
                            </li>
                            <li>
                                <p>
                                    Узнаете функции эмоций - о чем говорят эмоции и как эффективно, органично позволить
                                    эмоциям
                                    действовать и направлять вас на удовлетворение ваших потребностей, на исполнение
                                    ваших
                                    желаний;
                                </p>
                            </li>
                            <li>
                                <p>
                                    Научитесь следовать собственным убеждениям и не ориентироваться на чужое мнение;
                                </p>
                            </li>
                            <li>
                                <p>
                                    Узнаете как пользоваться своими эмоциями, чтоб влиять на свою жизнь и повысить
                                    уровень
                                    удовлетворения жизнью;
                                </p>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12  col-md-6">
                        <ul class="list-styled">
                            <li>
                                <p>
                                    Пересмотрите ваши убеждения по поводу счастья и от чего оно зависит (как часто
                                    бывает -
                                    успешный
                                    но, несчастливый);
                                </p>
                            </li>
                            <li>
                                <p>
                                    Поднимете уровень эмоционального интеллекта, научитесь обращаться со своими
                                    эмоциями:
                                    осознавать
                                    всю эмоциональную палитру, уметь определить их и “называть их по имени”;
                                </p>
                            </li>
                            <li>
                                <p>
                                    Научитесь выражать ваши эмоции структурировано, понятно, чтоб не прятать себя и не
                                    обидеть
                                    других;
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <h2>УСЛОВИЯ И СТОИМОСТЬ</h2>
                <div align="center" class="margin-top-50">
                    <p><strong>Длительность курса: 7 месяцев</strong></p>
                    <p>
                        (14 занятий по 6 часов), встречи по субботам 2 раза в месяц,<br>
                        с 10:00 до 18:00, перерыв на обед с 13:00-15:00
                    </p>
                </div>
                <div class="row justify-content-center">
                    <div class="box-course" align="center">
                        <div class="box-course-content">
                            <div class="box-course-price">
                                <h4>Стоимость одного занятия</h4>
                                <p class="price"><strong>3 000 сом</strong></p>
                            </div>
                            <div class="box-course-small">
                                <p>Итого за весь курс</p>
                                <p><strong>42 000 сом</strong></p>
                            </div>
                        </div>
                        <a href="javascript:void(0);"
                           class="btn btn-main-style padding-width-15" data-toggle="modal"
                           data-target="#callback-traning-1">Зарегистрироваться</a>
                    </div>
                    <div class="box-course" align="center">
                        <div class="box-course-content">
                            <div class="box-course-price">
                                <h4>Стоимость за месяц</h4>
                                <p class="price"><strong>5 500 сом</strong></p>
                                <p>(за два 6-ти часовых занятия)</p>
                            </div>
                            <div class="box-course-small">
                                <p>Итого за весь курс</p>
                                <p><strong>38 500 сом</strong></p>
                            </div>
                            <p class="discount margin-top-15">Скидка 8%</p>
                        </div>
                        <a href="javascript:void(0);"
                           class="btn btn-main-style padding-width-15" data-toggle="modal"
                           data-target="#callback-traning-2">Зарегистрироваться</a>
                    </div>
                    <div class="box-course" align="center">
                        <div class="box-course-content">
                            <div class="box-course-price">
                                <h4>При оплате за весь<br> курс сразу</h4>
                                <p>(14 занятий по 6 часов)</p>
                            </div>
                            <div class="box-course-small">
                                <p>Итого за весь курс</p>
                                <p><strong>36 000 сом</strong></p>
                            </div>
                            <p class="discount margin-height-10">Скидка 15%</p>
                        </div>
                        <a href="javascript:void(0);"
                           class="btn btn-main-style padding-width-15" data-toggle="modal"
                           data-target="#callback-traning-3">Зарегистрироваться</a>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <h2>КТО ВЕДЕТ КУРС?</h2>
                <div class="row margin-top-50">
                    <?php $model = \App\Models\Psychologists::find(1); ?>
                    <div class="col-md-6" align="center">
                        @include('components.psychologists.item_small', ['psycho'=> $model])
                    </div>
                    <?php $model = \App\Models\Psychologists::find(2); ?>
                    <div class="col-md-6" align="center">
                        @include('components.psychologists.item_small', ['psycho'=> $model])
                    </div>
                </div>
            </div>
        </section>


        <section>
            <div class="container">
                <h2>ОТЗЫВЫ</h2>
                <div class="review-main-container">
                    @include('components.blog.review', ['type'=>\App\Models\Services::TYPE_GROUP])
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <div class="blog-container">
                    @include('components.blog.photo',['type'=>\App\Models\Services::TYPE_GROUP])
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                @include('components.callback.callback', ['id'=>'form-callback-g','callback_type'=>\App\Models\Callbacks::TYPE_PAGE_GROUP])
            </div>
        </section>
    </div>
    @include('components.modals.modal_callback_traning', ['id'=>'callback-traning-1','callback_type'=>\App\Models\Callbacks::TYPE_TRAINING_1,
 'title'=>'«Позитивная психология» + Эмоциональный интеллект', 'sub_title'=>'Одно занятие'])
    @include('components.modals.modal_callback_traning', ['id'=>'callback-traning-2','callback_type'=>\App\Models\Callbacks::TYPE_TRAINING_2,
     'title'=>'«Позитивная психология» + Эмоциональный интеллект', 'sub_title'=>'Один месяц'])
    @include('components.modals.modal_callback_traning', ['id'=>'callback-traning-3','callback_type'=>\App\Models\Callbacks::TYPE_TRAINING_3,
      'title'=>'«Позитивная психология» + Эмоциональный интеллект', 'sub_title'=>'Полный курс'])
@endsection