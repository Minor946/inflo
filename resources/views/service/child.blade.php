<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 06.10.2018
 * Time: 16:45
 */

?>

@extends('layouts.layout')

@section('title', "Курс «Позитивная психология» Эмоциональный интеллект для подростков")

@section('content')
    <div id="full-page">
        <section class="padding-top-40">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="landing-label">
                            <h2>Курс «Позитивная психология</h2>
                            <h2>+</h2>
                            <h2>Эмоциональный интеллект<br>
                                для подростков»</h2>
                        </div>
                        <p class="text-muted">(занятия в группе)</p>
                    </div>
                    <div class="col-md-6">
                        <div class="video_wrapper video_wrapper_full js-videoWrapper">
                            <iframe class="videoIframe js-videoIframe"
                                    src="{{config('youtube_child')}}"
                                    frameborder="0" allowTransparency="true"
                                    width='475px' height='267px'>
                            </iframe>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-30">
                    <div class="col-md-6">
                        <p>Подростковый период - это очень важная часть взросления, подросток учится быть взрослым и у
                            него
                            есть две задачи: Первая - понять кто он или она есть; Вторая - отделиться от родителей; </p>
                    </div>
                    <div class="col-md-6">
                        <p>Подростку нужна поддержка родителя и пространство быть собой, совершать ошибки, учиться
                            принимать
                            важные решения самостоятельно. </p>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <h2>ЧТО ТАКОЕ ПОЗИТИВНАЯ ПСИХОЛОГИЯ</h2>
                <div class="row margin-top-50">
                    <div class="col-md-6 order-2 order-md-1 margin-top-50">
                        <p>Направление психологии, которое занимается исследованием положительных аспектов психики
                            человека.
                            В отличии от классической психологии, которая ориентируется в основном на проблемы и
                            патологии,
                            основными темами исследований позитивной психологии является то, что способствует достижению
                            счастья людей (например, оптимизм, поток, доверие, прощение и солидарность).</p>

                        <p>Эта область исследований стремится раскрыть природные способности человека и сделать опыт
                            жизни
                            более удовлетворительным и радостным.</p>
                    </div>
                    <div class="col-md-6 order-1 order-md-2">
                        <img src="{{url('/images/landing/child/child_1.png')}}" class="img-full">
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container section-child">
                <h2>ДЛЯ КОГО ЭТОТ КУРС</h2>
                <p align="center"><strong>Если тебе от 13 до 17 - этот курс для тебя!</strong></p>
                <div class="row margin-bottom-50">
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                        <img src="{{url('/images/landing/child/icons-2.png')}}" class="child-icon">
                        <p>
                            Вы сейчас закладываете фундамент своей личности - и будет это дом или небоскреб - решать
                            вам!
                            Главное, чтоб вы осознано делали выбор и создавали свою жизнь сами!
                        </p>
                    </div>
                    <div class="col-md-5">
                        <img src="{{url('/images/landing/child/icons-1.png')}}" class="child-icon">
                        <p>
                            В школе рассказывают про законы физики, химии, математики, анатомии… но не говорят о том,
                            как
                            обращаться со своим внутренним миром. В мире как-будто им нет места эмоциям.
                        </p>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="row margin-bottom-50">
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                        <img src="{{url('/images/landing/child/icons-3.png')}}" class="child-icon">
                        <p>
                            Понимать свой внутренний мир, знать свои потребности и свои желания. Учится постепенно не
                            ждать
                            реализации желания от взрослых, а начать реализовывать их самому/ой!
                        </p>
                    </div>
                    <div class="col-md-5">
                        <img src="{{url('/images/landing/child/icons-4.png')}}" class="child-icon">
                        <p>
                            Родители нас спрашивают поели мы или нет, хорошо ли чувствуем себя физически, но никто не
                            спрашивает - как ты чувствуешь себя внутри. Тебе радостно, грустно, одиноко, ты злишься или
                            ты
                            разочарован/а - все это как-будто не существует. Но это очень и очень важно!
                        </p>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="row margin-bottom-50">
                    <div class="col-md-12" align="center">
                        <p>
                            Это все то, что в игривой форме вместе с такими же как ты - ты можешь получить у нас!
                        </p>
                        <p>
                            Никаких лекций и нравоучений!
                        </p>
                        <p>
                            У нас не надо поднимать руку, отпрашиваться в туалет и называть нас на “Вы”.
                        </p>
                        <p>
                            Здесь Вы на ровне со взрослыми - настолько же важны и ценны!
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <h2>ЧЕМУ УЧИТ КУРС</h2>
                <div class="row justify-content-md-center">
                    <div class="col-md-4">
                        <ul class="list-styled">
                            <li>Как построить свою личность</li>
                            <li>Как узнать, что важно именно мне</li>
                            <li>Как быть, когда грустно, одиноко,
                                скучно и ничего не хочется
                            </li>
                            <li>Проблемы первого сексуального
                                опыта - и все, что я переживаю
                                или пережил/а
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="list-styled">
                            <li>Как не ошибиться с выбором карьеры</li>
                            <li>Как иметь твердую самооценку</li>
                            <li>Как справится со стыдом, страхом,
                                изоляцией
                            </li>
                            <li>Конфликты с родителями - хочется
                                свободы и хочется быть хорошим
                                ребенком
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-10">
                        <p align="center">
                            Минимальное кол-во пар -12, пары будут разделены в 2 разные группы для более продуктивной
                            работы, чтобы половинки друг друга не смущали, но при этом, одновременно прорабатывали одни
                            и те
                            же техники;
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container" align="center">
                <h2>УСЛОВИЯ И СТОИМОСТЬ</h2>
                <div class="margin-top-50">
                    <p><strong>Длительность курса: 10 недель</strong></p>
                    <p><strong>Стоимость курса: 25000 сом</strong></p>
                    <div class="box-child">
                        <p class="margin-top-30">
                            (10 занятия по 3 часа), встречи по субботам 4 раза в месяц,
                            Стоимость за одно занятие: <strong>2500 сом</strong>
                        </p>
                        <p>
                            При оплате за весь курс сразу – <strong>22 000сом</strong><br>
                            <strong><span class="discount">Скидка 15%</span></strong>
                        </p>
                        <a href="javascript:void(0);"
                           class="btn btn-main-style margin-height-20 padding-width-15" data-toggle="modal"
                           data-target="#callback-traning-7">Зарегистрироваться</a>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <h2>КТО ВЕДЕТ КУРС?</h2>
                <div class="row margin-top-50">
                    <?php $model = \App\Models\Psychologists::find(1); ?>
                    <div class="col-md-6" align="center">
                        @include('components.psychologists.item_small', ['psycho'=> $model])
                    </div>
                    <?php $model = \App\Models\Psychologists::find(2); ?>
                    <div class="col-md-6" align="center">
                        @include('components.psychologists.item_small', ['psycho'=> $model])
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <h2>ОТЗЫВЫ</h2>
                <div class="review-main-container">
                    @include('components.blog.review',['type'=>\App\Models\Services::TYPE_CHILD])
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <div class="blog-container">
                    @include('components.blog.photo',['type'=>\App\Models\Services::TYPE_CHILD])
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                @include('components.callback.callback', ['id'=>'form-callback-c','callback_type'=>\App\Models\Callbacks::TYPE_PAGE_CHILD])
            </div>
        </section>
    </div>
    @include('components.modals.modal_callback_traning', ['id'=>'callback-traning-7','callback_type'=>\App\Models\Callbacks::TYPE_TRAINING_7,
    'title'=>'«Позитивная психология» + Эмоциональный интеллект для подростков', 'sub_title'=>'Полный курс'])
@endsection
