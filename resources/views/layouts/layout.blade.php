<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta property="og:url" content="{{url($_SERVER['REQUEST_URI']) }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title-news')"/>
    <meta property="og:description" content="@yield('description')"/>
    <meta property="og:image" content="{{url('/storage/')}}@yield('image')"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Inflow - @yield('title')</title>
    <link rel="icon" type="image/png" href="{{{ url('/images/menu.png') }}}">
    <!-- Fonts -->
    <!-- Styles -->
    <link href="{{ url('/css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('/css/responsive.css') }}" rel="stylesheet" type="text/css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
</head>
<body>

<?php
$route = "";
use Illuminate\Support\Facades\Route;
if (!empty(Route::getFacadeRoot()->current())) {
    $route = Route::getFacadeRoot()->current()->uri();
}
?>

@section('menu')
    @include('layouts.menu')
@show
<?php if($route != 'about'):?>
@section('sidebar')
    @include('layouts.navbar')
@show
<?php endif;?>

<div class="wrapper" id="wrapper">

    <div id="page-content-wrapper" class="container-fluid">
        @yield('content')
    </div>

    @section('footer')
        @include('layouts.footer')
    @show
</div>
@yield('scripts')
@include('components.modals.modal_callback', ['id'=>'form-callback-modal','callback_type'=>\App\Models\Callbacks::TYPE_MAIN])
</body>

</html>

