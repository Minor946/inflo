<footer>
    <div class="footer">
        <div class="row">
            <div class="col-md">
                <h4>Телефон:</h4>
                <p>{{config('main_phone')}}</p>
                <h4>WhatsApp:</h4>
                <p>
                    <a href="https://api.whatsapp.com/send?phone=<?=str_replace('+', '', str_replace(' ', '', config('whatsapp')));?>"
                       target="_blank">{{config('whatsapp')}}</a></p>
            </div>
            <div class="col-md">
                <h4>Офис:</h4>
                <p>{{config('address')}}</p>
            </div>
            <div class="col-md">
                <h4>Skype:</h4>
                <p><a href="skype:profile_name?{{config('skype')}}" target="_blank">{{config('skype')}}</a></p>
            </div>
            <div class="col-md">
                <h4>Соц сети:</h4>
                <a href="{{config('instagram')}}" target="_blank"><img src="{{url('/images/icons/insta-b.svg')}}"></a>
                <a href="{{config('facebook')}}" target="_blank"><img src="{{url('/images/icons/facebook-b.svg')}}"></a>
                <a href="{{config('youtube')}}" target="_blank"><img src="{{url('/images/icons/youtube-b.svg')}}"></a>
            </div>
            <div class="col-md">
                <h4>Мы в мобильных приложениях:</h4>
                <div class="row">
                    <div class="col-md-6">
                        <a href="{{config('app-store')}}" target="_blank"><img
                                    src="{{url('/images/icons/app-store.svg')}}"
                                    class="footer-app"></a>
                    </div>
                    <div class="col-md-6">
                        <a href="{{config('google-pay')}}" target="_blank"><img
                                    src="{{url('/images/icons/google-play.svg')}}"
                                    class="footer-app"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="{{ url('js/app.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/notie/4.3.1/notie.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sticky-kit/1.1.3/sticky-kit.min.js" type="text/javascript"></script>
<script src="{{ url('js/scripts.js') }}" type="text/javascript"></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript"> (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter50903711 = new Ya.Metrika2({
                    id: 50903711,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true,
                    trackHash: true
                });
            } catch (e) {
            }
        });
        var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
            n.parentNode.insertBefore(s, n);
        };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks2"); </script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/50903711" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript> <!-- /Yandex.Metrika counter -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-128248628-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-128248628-1');
</script>
