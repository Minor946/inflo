<div class="drawer-menu">
    <div class="fab-btn">
      <span class="fab-action-button" id="toggle">
          <div class="fab-btn-block" align="center">
            <img src="{{url('/images/icons/menu.svg')}}" id="menu-pill">
          </div>
      </span>
    </div>
</div>

<div class="overlay" id="overlay">
    <nav class="overlay-menu container">
        <div class="row">
            <div class="col-md mob-hide">
                <div class="row nav box-menu-grid" id="menuTab" role="tablist" align="center">
                    <a class="box-menu" data-toggle="tab" href="#v-menu-1" id="v-menu-pill-1"
                       aria-controls="v-menu-1" role="tab" aria-selected="true">
                        <img src="{{url('/images/icons/icon-menu-1.svg')}}"><br>
                        <strong>Задать вопрос</strong>
                    </a>
                    <a class="box-menu" data-toggle="tab" href="#v-menu-3" id="v-menu-pill-3"
                       aria-controls="v-menu-3" role="tab" aria-selected="false">
                        <img src="{{url('/images/icons/icon-menu-3.svg')}}"><br>
                        <strong>Записаться на консультацию</strong>
                    </a>
                    <a class="box-menu" data-toggle="tab" href="#v-menu-2" id="v-menu-pill-2"
                       aria-controls="v-menu-2" role="tab" aria-selected="false">
                        <img src="{{url('/images/icons/icon-menu-2.svg')}}"><br>
                        <strong>Пройти тренинг онлайн</strong>
                    </a>
                    <a class="box-menu"
                       href="https://api.whatsapp.com/send?phone=<?=str_replace('+', '', str_replace(' ', '', config('whatsapp')));?>"
                       target="_blank">
                        <img src="{{url('/images/icons/icon-menu-4.svg')}}"><br>
                        <strong>Написать в WhatsApp</strong>
                    </a>
                </div>
                <div class="clearfix"></div>
                <div class="row margin-bottom-10">
                    <div class="col-md">
                        <a href="{{config('instagram')}}" target="_blank"><img
                                    src="{{url('/images/icons/instagram.svg')}}"></a>
                        <a href="{{config('facebook')}}" target="_blank"><img
                                    src="{{url('/images/icons/facebook.svg')}}"></a>
                    </div>
                </div>
                <p>{{config('address')}}</p>
                <p>{{config('main_phone')}}</p>
            </div>
            <div class="col-md">
                <h2 class="mob-hidden">Задать вопрос</h2>
                <div class="tab-content" id="v-pills-menuContent">
                    <div class="tab-pane fade show active" id="v-menu-1" role="tabpanel"
                         aria-labelledby="v-menu-pill-1">
                        <div class="callback-box-menu">
                            {!! Form::open(['url' => '/callback', 'id'=>'form-callback-q']) !!}
                            {{Form::token()}}
                            <div class="form-group">
                                {{ Form::text("name", "", array_merge(['class' => 'form-control',
                                 'placeholder'=>'ваше имя', 'required' => true])) }}
                            </div>
                            <div class="form-group">
                                {{ Form::text("phone", "", array_merge(['class' => 'form-control',
                                 'placeholder'=>'телефон', 'type'=>'phone', 'required' => true])) }}
                            </div>
                            <div class="form-group">
                                {{ Form::email("email", "", array_merge(['class' => 'form-control',
                                 'placeholder'=>'email', 'required' => true])) }}
                            </div>

                            <div class="form-group">
                                {{ Form::textarea("msg", "", array_merge(['class' => 'form-control',
                                  'placeholder'=>'сообщение',  'required' => true,'rows'=>5])) }}
                            </div>
                            {{ Form::hidden('type', 6)}}
                            <div align="center">
                                {{Form::submit('Отправить заявку',['class' => 'btn btn-main-style btn-submit margin-height-20'])}}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="v-menu-2" role="tabpanel"
                         aria-labelledby="v-menu-pill-2">
                        <div class="callback-box-menu">
                            {!! Form::open(['url' => '/callback', 'id'=>'form-callback-сon']) !!}
                            {{Form::token()}}
                            <div class="form-group">
                                {{ Form::text("name", "", array_merge(['class' => 'form-control',
                                 'placeholder'=>'ваше имя', 'required' => true])) }}
                            </div>
                            <div class="form-group">
                                {{ Form::text("phone", "", array_merge(['class' => 'form-control',
                                 'placeholder'=>'телефон', 'type'=>'phone', 'required' => true])) }}
                            </div>
                            <div class="form-group">
                                {{ Form::email("email", "", array_merge(['class' => 'form-control',
                                 'placeholder'=>'email', 'required' => true])) }}
                            </div>
                            {{ Form::hidden('type', 7)}}
                            <div align="center">
                                {{Form::submit('Отправить заявку',['class' => 'btn btn-main-style btn-submit margin-height-20'])}}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="v-menu-3" role="tabpanel"
                         aria-labelledby="v-menu-pill-3">
                        <div class="callback-box-menu">
                            {!! Form::open(['url' => '/callback',  'id'=>'form-callback-t']) !!}
                            {{Form::token()}}
                            <div class="form-group">
                                {{ Form::text("name", "", array_merge(['class' => 'form-control',
                                 'placeholder'=>'ваше имя', 'required' => true])) }}
                            </div>
                            <div class="form-group">
                                {{ Form::text("phone", "", array_merge(['class' => 'form-control',
                                 'placeholder'=>'телефон', 'type'=>'phone', 'required' => true])) }}
                            </div>
                            <div class="form-group">
                                {{ Form::email("email", "", array_merge(['class' => 'form-control',
                                'placeholder'=>'email', 'required' => true])) }}
                            </div>
                            {{ Form::hidden('type', 8)}}
                            <div align="center">
                                {{Form::submit('Записаться',['class' => 'btn btn-main-style btn-submit margin-height-20'])}}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <img src="{{url('/images/icons/close.png')}}" class="button_container" id="toggle-close">
</div>
