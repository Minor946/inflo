<nav class="navbar navbar-expand-lg fixed-top navbar-container navbar-light bg-transparent" id="main-navbar">
    <a class="navbar-brand mob-hide" href="{{ url('/') }}">
        <img src="{{url('/images/icons/logo.svg')}}"><br>
        <span class="brand-desc">ЦЕНТР<br> ПОЗИТИВНОЙ<br> ПСИХОЛОГИИ</span>
    </a>
    <a class="navbar-brand mob-hidden" href="{{ url('/') }}">
        <img src="{{url('/images/icons/logo-mob.svg')}}"><br>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <span class="navbar-text mr-auto"></span>
        <ul class="navbar-nav">
            <?php if($route != '/'):?>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/') }}">Главная</a>
            </li>
            <?php endif;?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Услуги
                </a>
                <div class="dropdown-menu dropdown-styled" aria-labelledby="navbarDropdownMenuLink">
                    <h6 class="dropdown-header">Позитивная психология</h6>
                    <a class="dropdown-item" href="{{ url('/service/group') }}">Групповые занятия</a>
                    <a class="dropdown-item" href="{{ url('/service/family') }}">Для семейных пар</a>
                    <a class="dropdown-item" href="{{ url('/service/child') }}">Для подростков</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/psychologists') }}">Психологи</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/blog') }}">Статьи</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/club') }}">Наш клуб</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/trainings') }}">Online тренинги</a>
            </li>
        </ul>
        <hr class="mob-hidden">
        <ul class="navbar-nav navbar-nav-icon">
            <li class="nav-item-icon">
                <a href="{{config('instagram')}}" target="_blank"><img src="{{url('/images/icons/instagram.svg')}}"></a>
            </li>
            <li class="nav-item-icon">
                <a href="{{config('facebook')}}" target="_blank"><img src="{{url('/images/icons/facebook.svg')}}"></a>
            </li>
            <li class="nav-item-icon">
                <a href="https://api.whatsapp.com/send?phone=<?=str_replace('+', '', str_replace(' ', '', config('whatsapp')));?>"
                   target="_blank"><img src="{{url('/images/icons/whatsapp.svg')}}"></a>
            </li>
        </ul>
    </div>
</nav>
