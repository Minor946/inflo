@extends('layouts.layout')

@section('title', "Главная страница")

@section('content')
    <div id="full-page">
        <section>
            @include('components.main.slider')
        </section>
        <section>
            @include('components.main.studios')
        </section>
        <section>
            @include('components.main.created')
        </section>
        <section>
            @include('components.main.can_get')
        </section>
        <section>
            @include('components.main.service_2')
        </section>
        <section>
            @include('components.main.service')
        </section>
        <section>
            <div class="container container-callback">
                @include('components.callback.callback', ['id'=>'form-callback-m','callback_type'=>\App\Models\Callbacks::TYPE_MAIN])
            </div>
        </section>
    </div>
@endsection

<style>
    #wrapper {
        margin-top: 50px !important;
        overflow-x: hidden;
    }
</style>