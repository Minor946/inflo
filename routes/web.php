<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [],
    function()
    {
        Route::get('/', 'MainController@index');

        Route::get('/psychologists', 'MainController@psychologists');

        Route::get('/service/{type}', 'MainController@service');

        Route::get('/about', 'MainController@about');

        Route::get('/blog', 'MainController@blog');
        Route::get('/blog/{alias}', 'MainController@blog');

        Route::get('/club', 'MainController@club');

        Route::get('/trainings', 'MainController@trainings');


        Route::post('/add-blogs', 'MainController@add');
        Route::post('/callback', 'MainController@callback');
    }
);
