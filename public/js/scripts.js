/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 22/09/18
 * Time: 10:05 PM
 */

$('#toggle').click(function () {
    $('#toggle-close').fadeIn();
    $('#overlay').toggleClass('open');
});

$('#toggle-close').click(function () {
    $(this).fadeOut();
    $('#overlay').toggleClass('open');
});

$(document).ready(function () {
    if(window.innerWidth <= 800 && window.innerHeight <= 700) {
        return true;
    } else {
        // $("#sticker").stick_in_parent();
        // $('#sticker').sticky({
        //     topSpacing: 5,
        //     bottomSpacing: 50,
        //     center: true,
        //     getWidthFrom:$('.row-blog')
        // });
    }
});


(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v3.1&appId=112431032852752';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function openCertificates(id) {
    var element = '.certificates' + id;
    var lightbox = $(element + ' a').simpleLightbox();
    lightbox.open()
}



$(document).ready(function () {

    $('.get-icon').hover(
        function () {
            $(this).find("p").toggleClass('slideInRight');
        }, function () {
            $(this).find("p").toggleClass('slideInRight');
        }
    );

    setInterval(function () {
        $('#menu-pill').toggleClass('menu-pill');
    }, 5000);

    function share_fb(url) {
        window.open('https://www.facebook.com/sharer/sharer.php?u=' + url, 'facebook-share-dialog', "width=626, height=436")
    }

    var mySwiperBlog = new Swiper('#slider-blog', {
        direction: 'horizontal',
        loop: true,
        pagination: {
            el: '.swiper-pagination',
        },
        navigation: {
            nextEl: '.control-btn-right',
            prevEl: '.control-btn-left',
        },
    });

    var mySwiperReview = new Swiper('#slider-review', {
        direction: 'horizontal',
        slidesPerView: 3,
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
        },
    });

    var mySwiperStudio = new Swiper('#slider-studio', {
        direction: 'horizontal',
        loop: true,
        pagination: {
            el: '.swiper-pagination',
        },
        navigation: {
            prevEl: '.flat-arrow-left',
            nextEl: '.flat-arrow-right',
        },
    });

    var mySwiperService = new Swiper('#slider-services', {
        direction: 'horizontal',
        loop: true,
        slidesPerView: 1,
        pagination: {
            el: '.swiper-pagination',
        },
        navigation: {
            prevEl: '.flat-arrow-left',
            nextEl: '.flat-arrow-right',
        },
    });

    var mySwiperMain = new Swiper('#slider-main', {
        direction: 'horizontal',
        effect: 'fade',
        autoplay: {
            delay: 3000,
        },
        pagination: {
            el: '.pagination-main',
        },
        navigation: {
            prevEl: '.flat-arrow-main-left ',
            nextEl: '.flat-arrow-main-right',
        },
    });

    mySwiperMain.on('slideChange', function () {
        $last = $('#main-slider img[data-slide="' + (mySwiperMain.previousIndex) + '"]');
        $last.attr("class", "animated fadeOutLeft");
        setTimeout(function () {
            $last.hide();
            $active = $('#main-slider img[data-slide="' + mySwiperMain.activeIndex + '"]');
            $active.show();
            $active.attr("class", "animated slideInRight");
        }, 200);
    });

    var mySwiperPopular = new Swiper('#slider-popular', {
        slidesPerView: 3,
        spaceBetween: 15,
        navigation: {
            prevEl: '.flat-arrow-left',
            nextEl: '.flat-arrow-right',
        },

    });

    var mySwiperPhoto = new Swiper('#slider-photo', {
        slidesPerView: 3,
        spaceBetween: 15,
        navigation: {
            prevEl: '.flat-arrow-left',
            nextEl: '.flat-arrow-right',
        },

    });

    $(window).scroll(function () {
        var height = $(window).scrollTop();
        if (height > 20) {
            $('.brand-desc').fadeOut();
            $('#main-navbar').addClass('bg-navbar');
        } else {
            $('.brand-desc').fadeIn();
            $('#main-navbar').removeClass('bg-navbar');

        }
    });

    $('.btn-psycho').on('click', function () {
        $('#modal-callback').toggleClass('open');
        return false;
    });

    $('.review-more').on('click', function () {
        var id = $(this).attr('data-value');
        $('#' + id).find('p').toggleClass('fill-text');
        if ($(this).html() === "читать далее") {
            $(this).html("срыть");
        } else {
            $(this).html("читать далее");
        }
        return false;
    });

    $(window).resize(function () {
        var ww = $(window).width();
        if (ww <= 768) {
            if ($('#slider-review').length) {
                mySwiperReview.params.slidesPerView = 1;
            }
            if ($('#slider-popular').length) {
                mySwiperPopular.params.slidesPerView = 1;
            }
            if ($('#slider-photo').length) {
                mySwiperPhoto.params.slidesPerView = 1;
            }
        } else {
            if ($('#slider-review').length) {
                mySwiperReview.params.slidesPerView = 3;
            }
            if ($('#slider-popular').length) {
                mySwiperPopular.params.slidesPerView = 3;
            }
            if ($('#slider-photo').length) {
                mySwiperPhoto.params.slidesPerView = 3;
            }
        }
        if ($('#slider-review').length) {
            mySwiperReview.update();
        }
        if ($('#slider-popular').length) {
            mySwiperPopular.update();
        }
        if ($('#slider-photo').length) {
            mySwiperPhoto.update();
        }
    });
    $(window).trigger('resize');
});

$(document).ready(function ($) {
    $('form[id*="form-callback-"]').on('submit', function (e) {
        $.ajax({
            type: "POST",
            url: "/callback",
            data: $(this).serializeArray(),
            cache: false,
            success: function (html) {
                if (html['status'] === "true") {
                    notie.alert({
                        type: 'info',
                        text: "Спасибо, мы Вам скоро перезвоним",
                        stay: false,
                        time: 6,
                        position: 'bottom'
                    });
                }
            }
        });
        e.preventDefault();
    });
});

$(document).on('click', '.close-tab', function (e) {
    $('#v-pills-tab a[data-toggle="tab"]').parent().removeClass('active');
    $('#v-pills-tabContent .tab-pane.active').removeClass('active');
    $("#first-tab").addClass("show active");
    $(this).fadeOut();
});

function showClose() {
    $('.close-tab').fadeIn();
}

$(document).on('click', '#add-more', function (e) {
    var inProgress = false;
    var limit = 6;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '/add-blogs',
        method: 'POST',
        cache: false,
        data: {"limit": limit},
        beforeSend: function () {
            inProgress = true;
        }
    }).done(function (data) {
        $("#blogContainer").append(data.data);
        inProgress = false;
        limit += 6;
        if (data.count < 6) {
            $('#add-more').fadeOut();
        }
    });
});